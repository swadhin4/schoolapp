
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
<head>
<meta charset="utf-8">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, height=device-height">

<body class="skin-blue sidebar-mini">
<div>
	<footer>
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<div class="widget">
					<h5 class="widgetheading">Get in touch with us</h5>
					<address>
					<strong>Bookscue Online</strong><br>
					 Sailashree Vihar<br>
					 Bhubaneswar, Odisha </address>
					<p>
						<i class="icon-phone"></i> (123) 456-7890 - (123) 555-7891 <br>
						<i class="icon-envelope-alt"></i> support@bookscue.com
					</p>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="widget">
					<h5 class="widgetheading">Pages</h5>
					<ul class="link-list">
						<li><a href="#">Terms and conditions</a></li>
						<li><a href="#">Privacy policy</a></li>
						<li><a href="#">Contact us</a></li>
					</ul>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="widget">
					<h5 class="widgetheading">Latest posts</h5>
					<ul class="link-list">
						<li><a href="#">Questions related to XII class Physics uploaded.</a></li>
						<li><a href="#">General Knowledge Subject Category is now available in Bookscue. </a></li>
						<li><a href="#">Student can now download Test progress report.</a></li>
					</ul>
				</div>
			</div>
			<div class="col-lg-3">
				<!-- <div class="widget">
					<h5 class="widgetheading">Flickr photostream</h5>
					<div class="flickr_badge">
						<script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=8&amp;display=random&amp;size=s&amp;layout=x&amp;source=user&amp;user=34178660@N03"></script>
					</div>
					<div class="clear">
					</div>
				</div> -->
			</div>
		</div>
	</div>
	<div id="sub-footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="copyright">
						<p>
							<span>&copy; Bookscue 2015 All right reserved. By </span><a href="http://www.bookscue.com" target="_blank">Bookscue Exams</a>
						</p>
					</div>
				</div>
				<div class="col-lg-6">
					<ul class="social-network">
						<li><a href="#" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#" data-placement="top" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="#" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
						<li><a href="#" data-placement="top" title="Google plus"><i class="fa fa-google-plus"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	</footer>
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<script src='<c:url value="/resources/js/jquery.easing.1.3.js"></c:url>'></script>
<script src='<c:url value="/resources/js/bootstrap.min.js"></c:url>'></script>
<script src='<c:url value="/resources/js/jquery.fancybox.pack.js"></c:url>'></script>
<script src='<c:url value="/resources/js/jquery.fancybox-media.js"></c:url>'></script>
<script src='<c:url value="/resources/js/jquery.fancybox-media.js"></c:url>'></script>
<script src='<c:url value="/resources/js/google-code-prettify/prettify.js"></c:url>'></script>
<script src='<c:url value="/resources/js/portfolio/jquery.quicksand.js"></c:url>'></script>
<script src='<c:url value="/resources/js/portfolio/setting.js"></c:url>'></script>
<script src='<c:url value="/resources/js/jquery.flexslider.js"></c:url>'></script>
<script src='<c:url value="/resources/js/animate.js"></c:url>'></script>
<script src='<c:url value="/resources/js/custom.js"></c:url>'></script>
</body>
</html>