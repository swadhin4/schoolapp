<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
<head>
<meta charset="utf-8">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, height=device-height">

<link href='<c:url value="/resources/css/bootstrap.min.css"></c:url>' rel="stylesheet" />
<link href='<c:url value="/resources/css/fancybox/jquery.fancybox.css"></c:url>' rel="stylesheet">
<link href='<c:url value="/resources/css/jcarousel.css"></c:url>' rel="stylesheet" />
<link href='<c:url value="/resources/css/flexslider.css"></c:url>' rel="stylesheet" />
<link href='<c:url value="/resources/css/style.css"></c:url>' rel="stylesheet" />
<link href='<c:url value="/resources/css/font-awesome-4.3.0/css/font-awesome.css"></c:url>' rel="stylesheet" />

<!-- Theme skin -->
<link href='<c:url value="/resources/skins/default.css"></c:url>' rel="stylesheet" />
<script src='<c:url value="/resources/js/jquery.js"></c:url>'></script>
<script src='<c:url value="/resources/js/angular.min.js"></c:url>'></script>
<script type="text/javascript">
	webContextPath = "${pageContext.request.contextPath}";
	$(document).ready(function() {
		
	})
	
	

	
	</script>
<style>
</style>
</head>
<%
response.setHeader("Cache-Control", "no-cache");
response.setHeader("Pragma", "no-cache");
response.setDateHeader("Expires", 0); 
%>
<body >

<c:set var="contextPath" value="${pageContext.request.contextPath}"></c:set>
<div id="wrapper">
	<!-- start header -->
	<header>
        <div class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><span>B</span>ookscue Online</a>
                     <h5><i>Designed to Test Your skills</i></h5>
                </div>
                <div class="navbar-collapse collapse ">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="${contextPath}/home"><i class="fa fa-home fa-fw"></i>Home</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Features <b class=" icon-angle-down"></b></a>
                        </li>
                        <li><a href="${contextPath}/"><i class="fa fa-book"></i>
                        Demo</a></li>
                        <li><a href="${contextPath}/about">About Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
	</header>
		
	<!-- <section class="callaction" style="background-color: #D5D5D5">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="big-cta">
					<div class="cta-text">
						<h2><span>Bookscue</span> Online Examination</h2>
					</div>
				</div>
			</div>
		</div>
	</div>
	</section>
	 -->
	</div>
	
</body>
</html>