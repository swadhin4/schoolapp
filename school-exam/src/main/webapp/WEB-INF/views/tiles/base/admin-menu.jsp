<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html ng-app="angularadminapp">
<head>
<meta charset="utf-8">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, height=device-height">

<spring:url value="/resources/css/bootstrap.min.css" 	var="resourceBootstrapCSSUrl" />
<spring:url value="/resources/css/font-awesome.min.css" 	var="resourceFontAwesomeCSSUrl" />
<spring:url value="/resources/css/AdminLTE.min.css" 	var="resourceAdminLTECSSUrl" />
<spring:url value="/resources/css/jquery-jvectormap-1.2.2.css" 	var="resourceJVectorCSSUrl" />
<spring:url value="/resources/css/ionicons.min.css" 	var="resourceIconeCSSUrl" />
<spring:url value="/resources/css/style.css" var="resourceStyleCSSUrl" />

<link rel="stylesheet" href="${resourceBootstrapCSSUrl}">
<link rel="stylesheet" href="${resourceStyleCSSUrl}">
<!-- Font-Awesome -->
<link rel="stylesheet" href="${resourceFontAwesomeCSSUrl}">
<!-- Ionicons -->
<link href="${resourceIconeCSSUrl}" rel="stylesheet"
	type="text/css" />
<!-- jvectormap -->
<link href="${resourceJVectorCSSUrl}" rel="stylesheet"
	type="text/css" />
<!-- Theme style -->
<link href="${resourceAdminLTECSSUrl}" rel="stylesheet"
	type="text/css" />
	<link href="resources/css/_all-skins.min.css" rel="stylesheet"
	type="text/css" />

<script type="text/javascript">
	webContextPath = "${pageContext.request.contextPath}";
</script>
</head>
<body class="skin-blue sidebar-mini">
<c:set var="contextPath" value="${pageContext.request.contextPath}"></c:set>
		
		
</body>
</html>