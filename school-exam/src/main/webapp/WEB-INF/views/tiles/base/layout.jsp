<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bookscue Examination</title>
<!-- 
<link rel="stylesheet" href="resources/css/bootstrap.min.css">
<link rel="stylesheet" href="resources/css/style.css">
Font-Awesome
<link rel="stylesheet" href="resources/css/font-awesome.min.css">
Ionicons
<link href="resources/css/ionicons.min.css" rel="stylesheet"
	type="text/css" />
jvectormap
<link href="resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet"
	type="text/css" />
Theme style
<link href="resources/css/AdminLTE.min.css" rel="stylesheet"
	type="text/css" />
	   <link href="resources/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css" /> -->
</head>
<body style="background-color:  #fff">
	<div class="row">
		<tiles:insertAttribute name="header"></tiles:insertAttribute>
	</div>
	<div class="row">
		  <div class="col-md-12">
			<tiles:insertAttribute name="body"></tiles:insertAttribute>
		</div>
	</div>	
	<div class="row">
		<tiles:insertAttribute name="footer"></tiles:insertAttribute>
	</div>	
</body>

	

</html>