<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, height=device-height">

<link href='<c:url value="/resources/css/bootstrap.min.css"></c:url>'
	rel="stylesheet" />
<link
	href='<c:url value="/resources/css/fancybox/jquery.fancybox.css"></c:url>'
	rel="stylesheet">
<link href='<c:url value="/resources/css/jcarousel.css"></c:url>'
	rel="stylesheet" />
<link href='<c:url value="/resources/css/flexslider.css"></c:url>'
	rel="stylesheet" />
<link href='<c:url value="/resources/css/style.css"></c:url>'
	rel="stylesheet" />
<link
	href='<c:url value="/resources/css/font-awesome-4.3.0/css/font-awesome.css"></c:url>'
	rel="stylesheet" />
	
<%-- 	<link href='<c:url value="/resources/css/app.css"></c:url>'
	rel="stylesheet" />
	<link href='<c:url value="/resources/css/iids.css"></c:url>'
	rel="stylesheet" /> --%>
	<%-- <link href='<c:url value="/resources/css/bootstrap.min.css"></c:url>'
	rel="stylesheet" /> --%>

<!-- Theme skin -->
<link href='<c:url value="/resources/skins/default.css"></c:url>'
	rel="stylesheet" />
<script src='<c:url value="/resources/js/jquery.js"></c:url>'></script>
<script src='<c:url value="/resources/js/angular.min.js"></c:url>'></script>
<script src='<c:url value="/resources/js/jquery.bootpag.min.js"></c:url>'></script>
<script type="text/javascript">
	webContextPath = "${pageContext.request.contextPath}";
	$(document).ready(function() {
		$('ul.nav li.dropdown').hover(function() {
			  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
			}, function() {
			  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
			});
		
		$('#profileLink').click(function(){
			$('#myprofile').click();
		});
	})
</script>
<style>
	.navar-brand{
	    color: #fff;
	}
	#usernameview{
	color:#fff
	}
	
</style>
</head>
<body>
	<c:set var="contextPath" value="${pageContext.request.contextPath}"></c:set>
	<div id="wrapper">
		<!-- start header -->
		<header>
			<nav class="navbar navbar-default navbar-static-top">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse"
							data-target=".navbar-collapse">
							<span class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="${contextPath}/home/user"><span>B</span>ookscue
							Online</a>
						<h5>
							<i style="color:#fff">Designed to Test Your skills</i>
						</h5>
					</div>
					 <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav">
							<li class="active"><a href="${contextPath}/home"><i
									class="fa fa-home fa-fw"></i>Home</a></li>
								<sec:authorize access="isAuthenticated()">
									<li class="dropdown">
										<a href class="dropdown-toggle" id="usernameview"
										data-toggle="dropdown"></b>
										<sec:authentication	property="principal.username" />
										</a>
										<ul class="dropdown-menu" style="margin-left: 28px;background-color: #ccc">
											<li><a href id="profileLink" >My Profile</a></li>
											<li> <a href="${contextPath}/logout/success">Logout</a></li>
											
										</ul>
									</li>
							</sec:authorize>
						</ul>
					</div>
				</div>
			</nav>
		</header>
	</div>
</body>
</html>