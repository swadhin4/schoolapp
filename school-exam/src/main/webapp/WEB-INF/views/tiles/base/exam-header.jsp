<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
<head>
<meta charset="utf-8">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, height=device-height">

<link href='<c:url value="/resources/css/bootstrap.min.css"></c:url>' rel="stylesheet" />
<link href='<c:url value="/resources/css/fancybox/jquery.fancybox.css"></c:url>' rel="stylesheet">
<link href='<c:url value="/resources/css/jcarousel.css"></c:url>' rel="stylesheet" />
<link href='<c:url value="/resources/css/flexslider.css"></c:url>' rel="stylesheet" />
<link href='<c:url value="/resources/css/style.css"></c:url>' rel="stylesheet" />
<link href='<c:url value="/resources/css/font-awesome-4.3.0/css/font-awesome.css"></c:url>' rel="stylesheet" />

<!-- Theme skin -->
<link href='<c:url value="/resources/skins/default.css"></c:url>' rel="stylesheet" />
<script src='<c:url value="/resources/js/jquery.js"></c:url>'></script>
<script src='<c:url value="/resources/js/angular.min.js"></c:url>'></script>
<script src='<c:url value="/resources/js/timer.js"></c:url>'></script>
<script type="text/javascript">
	webContextPath = "${pageContext.request.contextPath}";
	
	</script>
<style>
#clockdiv{
	font-family: sans-serif;
	color: #fff;
	display: inline-block;
	font-weight: 100;
	text-align: center;
	font-size: 30px;
}

#clockdiv > div{
	padding: 5px;
	border-radius: 3px;
	background: #000;
	display: inline-block;
}	

#clockdiv div > span{
	padding: 10px;
	border-radius: 3px;
	background: #000;
	display: inline-block;
}

.smalltext{
	padding-top: 5px;
	font-size: 16px;
}
</style></head>
<body>
<c:set var="contextPath" value="${pageContext.request.contextPath}"></c:set>
<div id="wrapper">
	<!-- start header -->
	<header>
        <div class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><span>B</span>ookscue Online</a>
                     <h5><i>Designed to Test Your skills</i></h5>
                </div>
               <div class="container">
               		
                   <div id="clockdiv" class="pull-right">
                   <h4 class="heading">Time Remaining</h4>
							<div style="display:none">
								<span class="days"></span>
								<div class="smalltext">Days</div>
							</div>
							<div>
								<span class="hours"></span>
								<div class="smalltext">Hours</div>
							</div>
							<div>
								<span class="minutes"></span>
								<div class="smalltext">Minutes</div>
							</div>
							<div>
								<span class="seconds"></span>
								<div class="smalltext">Seconds</div>
							</div>
						</div>
                </div>
            </div>
        </div>
	</header>
	</div>
	
</body>
</html>