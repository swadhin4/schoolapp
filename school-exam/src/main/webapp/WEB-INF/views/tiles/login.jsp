<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<title>Home</title>

<spring:url value="/resources/css/bootstrap.min.css"
	var="resourceBootstrapCSSUrl" />
<spring:url value="/resources/css/AdminLTE.min.css"
	var="resourceAdminLTECSSUrl" />
<spring:url value="/resources/css/jquery-jvectormap-1.2.2.css"
	var="resourceJVectorCSSUrl" />
<spring:url value="/resources/css/ionicons.min.css"
	var="resourceIconeCSSUrl" />
<spring:url value="/resources/css/style.css" var="resourceStyleCSSUrl" />
<spring:url value="/resources/css/_all-skins.min.css"
	var="resourceStyleSkinCSSUrl" />

<link rel="stylesheet" href="${resourceBootstrapCSSUrl}">
<link rel="stylesheet" href="${resourceStyleCSSUrl}">
<!-- Font-Awesome -->
<!-- Ionicons -->
<link href="${resourceIconeCSSUrl}" rel="stylesheet" type="text/css" />
<!-- jvectormap -->
<link href="${resourceJVectorCSSUrl}" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="${resourceAdminLTECSSUrl}" rel="stylesheet" type="text/css" />
<link href="${resourceStyleSkinCSSUrl}" rel="stylesheet" type="text/css" />

<spring:url value="/resources/js/jQuery-2.1.4.min.js"
	var="resourceJqueryUrl" />
<spring:url value="/resources/js/bootstrap.min.js"
	var="resourceBootstrapUrl" />
<spring:url value="/resources/js/fastclick.min.js"
	var="resourceFastClickUrl" />
<spring:url value="/resources/js/app.min.js" var="resourceAppminUrl" />
<spring:url value="/resources/js/jquery.validate.min.js"
	var="resourceJQValidationJSUrl" />
<spring:url value="/resources/js/register-controller.js"
	var="registerControllerJSUrl" />

<script src="${resourceJqueryUrl}" type="text/javascript"></script>
<script src="${resourceBootstrapUrl}" type="text/javascript"></script>
<script src="${resourceFastClickUrl}" type="text/javascript"></script>
<script src="${resourceAppminUrl}" type="text/javascript"></script>
<script type="text/javascript" src="${resourceJQValidationJSUrl}"></script>
<script type="text/javascript" src="${registerControllerJSUrl}"></script>
<script>
	$(document).ready(function() {
		$('*').mousedown(function(event) {
			if ($('.alert').is(":visible")) {
				$('.alert').hide();
			}
		});
	});
</script>

<style>
.btn-label {
	position: relative;
	left: -12px;
	display: inline-block;
	padding: 6px 12px;
	background: rgba(0, 0, 0, 0.15);
	border-radius: 3px 0 0 3px;
}

.btn-labeled {
	padding-top: 10;
	padding-bottom: 10;
}

.input-group {
	margin-bottom: 10px;
}

.separator {
	border-right: 1px solid #dfdfe0;
}

.successGreen {
	background-color: green;
}

.errorRed {
	background-color: red;
}

#exampleModalLabel {
	color: #fff
}

.alert-fixed-top {
	z-index: 1050 !important;
}

.fa-asterisk {
	color: red;
	font-size: 10px;
}
</style>
</head>
<%
response.setHeader("Cache-Control", "no-cache");
response.setHeader("Pragma", "no-cache");
response.setDateHeader("Expires", 0); 
%>
<body ng-app="loginApp">
	<c:set var="contextPath" value="${pageContext.request.contextPath}"></c:set>
	<section style="background-image:url('${contextPath}/resources/images/demo-background.jpg')">
		<div class="container" ng-controller="registerController">
			<div class="row">
				<div id="login-overlay" class="modal-dialog" style="width: 100%">

					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" id="myModalLabel">
								Skill Test @ <b>Bookscue</b>
							</h4>
							<%-- <c:if test="${status == '201' }">
								<div class="alert alert-danger" role="alert"
									id="invalidLoginalert">${errormessage}</div>
							</c:if> --%>
							${errormessage}
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-md-12">


									<div class="col-md-4">
										<img src="${contextPath}/resources/images/quiz.jpg"
											style="width: 142%"></img>
									</div>
									<div class="tab-content ">
										<div class="tab-pane active" id="registerWindow"
											>
											<div class="col-xs-12">
												<form id="registerForm" name="registerForm" role="form" ng-submit="doSubmit()" novalidate>
													<div class="col-md-12">
														<p class="lead">
															Enter your personal details to register to <span
																class="text-success">Bookscue.com</span>
														</p>
														<div class="col-md-6">
															<div class="input-group">
																<span class="input-group-addon"><span
																	class="fa fa-user"></span></span>
																<input type="text" class="form-control" name="firstname"
																	ng-model="user.firstName"  ng-minlength="4"  placeholder="Enter firstname"
																	required="required" />
															</div>
															<span style="color:red" ng-show="registerForm.firstname.$error.minlength">Firstname is too short.</span>
															<p style="color:red" ng-show="registerForm.firstname.$invalid && !registerForm.firstname.$pristine" class="help-block">You name is required.</p>		
															
														</div>
														<div class="col-md-6">
															<div class="input-group">
																<span class="input-group-addon"><span
																	class="fa fa-user"><i class="fa fa-asterisk"></i></span></span>
																<input type="text" class="form-control" name="lastname"
																	ng-model="user.lastName" placeholder="Enter lastname"
																	required />
															</div>
															<p style="color:red" ng-show="registerForm.lastname.$invalid && !registerForm.lastname.$pristine" class="help-block">Lastname is required.</p>		
															
														</div>
													</div>
													<div class="col-md-12">
														<div class="col-md-12">
															<div class="input-group">
																<span class="input-group-addon"><span
																	class="fa fa-university"><i
																		class="fa fa-asterisk"></i></span></span> <input type="text"
																	class="form-control" name="institutename"
																	ng-model="user.instituteName"
																	placeholder="Enter your school name."
																	required />
															</div>
															<p style="color:red" ng-show="registerForm.institutename.$invalid && !registerForm.institutename.$pristine" class="help-block">School Name is required.</p>		
															
														</div>
													</div>
													<div class="col-md-12">
														<div class="col-md-6">
															<div class="input-group">
																<span class="input-group-addon"><span
																	class="fa fa-university"><i
																		class="fa fa-asterisk"></i></span></span> <select name="boardnames"
																	id="boardnames" class="form-control"
																	ng-change="getBoardStandard(boardName.selected)"
																	ng-model="boardName.selected"
																	ng-options="obj.name as obj.name for obj in boardName.values" required>
																	<option value="">---Select Board--</option>
																</select>
																
															</div>
															<p style="color:red" ng-show="registerForm.boardnames.$invalid && !registerForm.boardnames.$pristine" class="help-block">Board name is required.</p>		
															
														</div>
														<div class="col-md-6">
															<div class="input-group">
																<span class="input-group-addon"><span
																	class="fa fa-info-circle"><i
																		class="fa fa-asterisk"></i></span></span> <select name="standarnames"
																	id="standarnames" class="form-control"
																	ng-model="standard.selected"
																	ng-options="class.name for class  in standard.values">
																	<option value="">---Select Class--</option>
																</select>
																
															</div>
																<p style="color:red" ng-show="registerForm.standarnames.$invalid && !registerForm.standarnames.$pristine" class="help-block">Standard/Class name is required.</p>		
													
														</div>
													</div>

													<div class="col-md-12">
														<div class="col-md-6">
															<div class="input-group">
																<span class="input-group-addon"><span
																	class="fa fa-mobile"><i class="fa fa-asterisk"></i></span></span>
																<input ng-model="user.mobile" type="number" 
																	class="form-control" name="mobile"
																	placeholder="Enter Mobile" ng-minlength="10" ng-maxlength="10" required/>
															</div>
															<p style="color:red" ng-show="registerForm.mobile.$error.minlength" class="help-block">Invalid mobile number</p>
															<p style="color:red" ng-show="registerForm.mobile.$error.number" class="help-block">Not a valid number</p>
															<p style="color:red" ng-show="registerForm.mobile.$invalid && !registerForm.mobile.$pristine" class="help-block">Mobile number is required.</p>		
															
														</div>
														<div class="col-md-6">
															<div class="input-group">
																<span class="input-group-addon"><span
																	class="fa fa-at"></span></span> <input ng-model="user.email"
																	type="text" class="form-control" name="email"
																	placeholder="Enter Email" />
															</div>
														</div>
													</div>
													<div class="col-md-12">
														<div class="col-md-6">
															<div class="input-group">
																<label>Do you have a coupon code ? </label>
															</div>
														</div>
														<div class="col-md-6">
															<div class="input-group">
																<span class="input-group-addon"><span
																	class="fa fa-at"></span></span><input
																	ng-model="user.couponCode" type="text"
																	class="form-control" name="couponCode"
																	placeholder="Enter Copoun code" />
															</div>
														</div>
													</div>
													<div class="col-md-12">
														<div class="col-md-6">
															<label>Already have an account?</label> Click here to <label><a
																href="#loginWindow" data-toggle="tab" id="loginBtn">Login</a></label>
														</div>
														<div class="col-md-6">
															<button type="submit" id="registerbtn" ng-disabled="registerForm.$invalid"
																class="btn btn-labeled btn-primary pull-right">
																Register
															</button>
														</div>
													</div>
												</form>
											</div>


										</div>
										<!-- <div class="col-xs-6">
												<p class="lead">
													Login to <span class="text-success">BOOKSCUE.COM</span>
												</p>
												<ul class="list-unstyled" style="line-height: 2">
													<li><span class="fa fa-check text-success"></span>
														Test you skills</li>
													<li><span class="fa fa-check text-success"></span>
														Improve your IQ</li>
													<li><span class="fa fa-check text-success"></span>
														View your score card</li>
													<li><span class="fa fa-check text-success"></span> Get
														Discount Coupon</li>
													<li><span class="fa fa-check text-success"></span>
														Share Your Feedback</li>
												</ul>
												<a href="#2" data-toggle="tab"
													class="btn btn-success btn-block">Login</a>
												<button type="submit" value="Register" name="submit"
													class="btn btn-success btn-block">Register</button>
											</div> -->

										<div class="tab-pane" id="loginWindow">
											<div class="col-xs-6">
												<p class="lead">
													Sign-in to <span class="text-success">Bookscue.com</span>
												</p>
												<div class="well">
													<form id="loginForm" name="loginForm" novalidate action="${contextPath}/j_spring_security_check" method="POST">
													
														<div class="form-group">
															<label for="username" class="control-label">Username</label>
															<input type="text" class="form-control" name="j_username"
																ng-model="loginuser.username" placeholder="Enter username"
																required />
																
														</div>
														<p style="color:red" ng-show="loginForm.username.$invalid && !loginForm.mobile.$pristine" class="help-block">Username is required.</p>	
														<div class="form-group">
															<label for="password" class="control-label">Password</label>
															<input type="password" class="form-control" ng-model="loginuser.password" 
																name="j_password" placeholder="Please enter your password" required>
															<span class="help-block"></span>
														</div>
															<p style="color:red" ng-show="loginForm.password.$invalid && !loginForm.password.$pristine" class="help-block">Password is required.</p>		
														<button type="submit" value="login" name="submit" ng-disabled="loginForm.$invalid"
															class="btn btn-primary btn-block">Login</button>
													</form>
												</div>
											</div>
											<div class="col-xs-6">
												<p class="lead">
													Register now for <span class="text-success">FREE</span>
												</p>
												<ul class="list-unstyled" style="line-height: 2">
													<li><span class="fa fa-check text-success"></span>
														Test you skills</li>
													<li><span class="fa fa-check text-success"></span>
														Improve your IQ</li>
													<li><span class="fa fa-check text-success"></span>
														View your score card</li>
													<li><span class="fa fa-check text-success"></span> Get
														Discount Coupon</li>
													<li><span class="fa fa-check text-success"></span>
														Share Your Feedback</li>
												</ul>
												<label>To create an account, Click here </label> <a
													href="#registerWindow" data-toggle="tab">Join Now</a>
												<!-- <button type="submit" value="Register" name="submit"
													class="btn btn-success btn-block">Register</button> -->
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


			</div>
		</div>

		<button type="button" class="btn btn-primary" style="display: none"
			id="confirmBtn" data-toggle="modal" data-target="#exampleModal"></button>
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
			aria-labelledby="exampleModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header" id="headerdiv">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="exampleModalLabel">Confirmation</h4>
					</div>
					<div class="modal-body">
						<form>
							<div class="form-group">
								<label for="recipient-name" class="control-label"
									id="credentials"></label>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<div id="errordiv" class="alert alert-error alert-fixed-top"
			style="display: none;">
			<button type="button" class="close" data-dismiss="modal"
				onclick="$('.alert').hide()">x</button>
			<strong style="align: center;"><h3 style="color: white"
					id="errorDivText"></h3></strong>
		</div>

		<div id="successdiv" class="alert alert-success alert-fixed-top"
			style="display: none;">
			<button type="button" class="close" data-dismiss="modal"
				onclick="$('.alert').hide()">x</button>
			<strong style="align: center;"><h3 style="color: white"
					id="successDivText"></h3></strong>
		</div>

	</section>
</body>
</html>
