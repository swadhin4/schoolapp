<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<title>Home</title>

<spring:url value="/resources/css/dataTables.bootstrap.css"
	var="resourceDataTableCSSURL" />

<link rel="stylesheet" href="${resourceDataTableCSSURL}">


<script>
var userTable = null;
var userId = null;
	$(document)
			.ready(
					function() {
					
						loadEmployeeTable();
						loadUserTable();
					});
	
	function loadEmployeeTable(){
		$('#employeetable').DataTable({
			"ajax" : {
				"url" : webContextPath
						+ "/employee/info",
				"dataSrc" : function(json) {
					console.log(json);
					return json;
				}
			},
			"columns" : [
					{
						"data" : "id",

					},
					{
						"data" : "firstName"
					},

					{
						"data" : "lastName"
					},
					{
						"data" : "address.phone"
					},
					
					{
						"data" : function(data, type, row ) {
							return '<input type="checkbox" id="'+data.id+'" value="'+data.id+'" name="checkbox" class="checkbox">'
						},
					} ]
		});
		
	}
	
	function loadUserTable(){
		$('#usertable')
				.DataTable(

						{
							"ajax" : {
								"url" : webContextPath
										+ "/user/details",
								"dataSrc" : function(json) {
									console.log(json);
									return json;
								}
							},
							"columns" : [
									{
										"data" : "id",

									},
									{
										"data" : "firstName"
									},

									{
										"data" : "userName"
									},
									{
										"data" : "phone"
									},
									{
										"data" : "enabled"
									},
									
									{
										"data" : function(data, type, row) {
											return '<p data-placement="top" data-toggle="tooltip" title="Delete">'
													+ '<button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" onclick="getRowDeleteId('+data.id+')">'
													+ '<span class="glyphicon glyphicon-trash"></span></button></p>'
										}
									} ]
						});

		usertable = $('#usertable').DataTable();
		
		$('#usertable tbody').on('click', 'tr', function() {
			var rowData = usertable.row(this).data();
			// alert(rowData.id);
			userId = rowData.id;
			editUser(userId);
		});
		
	}
	
	function getSelectedCheckbox(){
		var user={};
		user.selectedEmployees=[];
		 $("input[type=checkbox]").each(function() {
			  var $this=$(this);
			  if($this.is(":checked")){
				  user.selectedEmployees.push($this.attr("id"));
			    }else{
			    }
			  
			});
		 
		 if(user.selectedEmployees.length>0){
			 var users=user.selectedEmployees;
			 $.ajax({
				 url: webContextPath+"/user/create?users="+users,
				 type:"POST",
				 success:function(data){
					 alert("User Successfully Created");
				 }
			 });
			 
		 }
		 else{
			 
		 }
	}

	function editUser(userid) {
		$
				.ajax({
					url : webContextPath + "/user/edit/" + userid,
					type : 'GET',
					success : function(data) {
						console.log(JSON.stringify(data.object));
						document.getElementById('employeeid').value = data.object.id;
						document.getElementById('firstName').value = data.object.firstName;
						document.getElementById('lastName').value = data.object.lastName;
						document.getElementById('phone').value = data.object.phone;
						document.getElementById('address').value = data.object.address;
						$('#submit').val("Update");
					}
				});

	}
</script>
</head>
<body>
	<section style="background-color: lightgray; margin-top: 10px">
		<c:set var="contextPath" value="${pageContext.request.contextPath}"></c:set>
		<div class="row">
			<div class="col-md-2">
				<aside class="main-sidebar">
					<section class="sidebar">

					<ul class="sidebar-menu" style="margin-top: 40px">
						<li class="active">
						<li><a href="${contextPath}/businessunits/details"><i
										class="fa fa-circle-o"></i>Business Units</a></li>
										
							<li><a href="${contextPath}/businessunits/firms"><i
									class="fa fa-circle-o"></i>Firm</a></li>	
							
						<li class="treeview"><a href="${contextPath}/vendor/details"><i
								class="fa fa-files-o"></i> <span>Vendors</span> 
								<span class="label label-primary pull-right">3</span> </a></li>
						
						
						<li class="treeview"><a href="${contextPath}/warehouse">
								<i class="fa fa-files-o"></i> <span>Warehouse</span> <!--  Display All Warehouse -->
								<span class="label label-primary pull-right">3</span>
						</a></li>
									
							<li><a href="${contextPath}/employee/list"><i
										class="fa fa-circle-o"></i>Employees</a></li>	
								<li><a href="${contextPath}/user/"><i
										class="fa fa-circle-o"></i> Users</a></li>
								
									
						
						<li class="treeview"><a href="${contextPath}/customers"><i
								class="fa fa-files-o"></i> <span>Customers</span> <!--  Display All Active Clients -->
								<span class="label label-primary pull-right">3</span> </a></li>
						
						<li class="treeview"><a href="${contextPath}/inventory"><i
								class="fa fa-files-o"></i> <span>Inventory</span> <!--  Display All Warehouse -->
								<span class="label label-primary pull-right">3</span> </a></li>
						<li class="treeview"><a href="#"> <i
								class="fa fa-files-o"></i> <span>Reports</span> <!--  Display All Warehouse -->
								<span class="label label-primary pull-right">3</span>
								<ul class="treeview-menu">
									<li><a href="index.html"><i class="fa fa-circle-o"></i>Daily
											Report</a></li>
									<li class="active"><a href="index2.html"><i
											class="fa fa-circle-o"></i>Monthly Report</a></li>
									<li><a href="index2.html"><i class="fa fa-circle-o"></i>Weekly
											Report</a></li>
									<li><a href="index2.html"><i class="fa fa-circle-o"></i>Quaterly
											Report</a></li>
									<li><a href="index2.html"><i class="fa fa-circle-o"></i>Semi-Annually
											Report</a></li>
									<li><a href="index2.html"><i class="fa fa-circle-o"></i>Annual
											Report</a></li>
									<li><a href="index2.html"><i class="fa fa-circle-o"></i>Client
											Based Report</a></li>
									<li><a href="index2.html"><i class="fa fa-circle-o"></i>Vendor
											Based Report</a></li>
								</ul>
						</a></li>
					</ul>
					</section>
					<!-- /.sidebar -->
				</aside>
			</div>
			<div class="col-md-10">
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title" id="monthlyname">User Creation Process</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-minus"></i>
							</button>
						</div>

					</div>

					<div class="box-body">
						<div class="container">
							<div class="col-md-12">
								<div class="form-group">
									<div class="row">
											<input type="hidden" name="employeeid" id="employeeid"
												value="" />
											<div class="col-sm-11">
								<table id="employeetable" class="table table-bordered table-hover"
								style="border: 1px solid lightgray" s>
								<thead>
									<tr>
										<th>ID</th>
										<th>First Name</th>
										<th>Last Name</th>
										<th>Phone Number</th>
										<th>Select</th>
									</tr>
								</thead>

							</table>
											</div>
									</div>
									<div class="row">
									<div class="col-md-2">
										<input type="button" name="submit" id="submit" value="Create User" onclick="getSelectedCheckbox();"
											class="btn btn-primary pull-left">
									</div>
									</div>

								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-10">
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">Users Created</h3>
					</div>
					<div class="box-body no-padding">
						<div class="box-body">
							<table id="usertable" class="table table-bordered table-hover"
								style="border: 1px solid lightgray">
								<thead>
									<tr>
										<th>ID</th>
										<th>First Name</th>
										<th>Username</th>
										<th>Phone Number</th>
										<th>Enabled</th>
										<th>Delete</th>
									</tr>
								</thead>

							</table>
						</div>
						<!-- /.table-responsive -->
					</div>
				</div>
				<!-- /.box-body -->
			</div>
		</div>

	</section>
</body>
</html>
