<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false"%>
<html>
<head>
<title>Home</title>
<script src='<c:url value="/resources/js/admin-controller.js"></c:url>'></script>

<script src='<c:url value="/resources/js/odia.js" ></c:url>'
	charset="utf-8"></script>

<script src='<c:url value="/resources/js/odia-script.js"></c:url>'></script>
<link href='<c:url value="/resources/css/OdiyaStyle.css"></c:url>'
	rel="stylesheet" />
<style>
#divBody label{
color:#fff
}
</style>	
</head>
<body ng-app="adminApp">
	<c:set var="contextPath" value="${pageContext.request.contextPath}"></c:set>
	<section id="content">
		<div class="container" ng-controller="adminController">
			<div class="row">
				<div class="col-lg-12">
					<div class="row">
						<div class="box">
							<div class="box-gray">
								<div class="row">
									<div class="col-md-2">
										<label class="control-lable">Board</label>
										<select name="boardname" class="form-control"
											ng-model="selected.board" ng-change="getSubjects()">
											<option value="" selected="selected">Select Board</option>
											<option ng-repeat="board in allBoards" value="{{board}}">
												{{board.name}}</option>
										</select>
									</div>
									<div class="col-md-3">
									<label class="control-lable">Subjects</label>
										<select name="subjectname" class="form-control"
											ng-model="selectedSubject.name" ng-change="getTopics()">
											<option value="" selected="selected">Select Subjects</option>
											<option ng-repeat="subject in allSubjects"
												value="{{subject}}">{{subject.name}}</option>
										</select>
									</div>
									<div class="col-md-4">
									<label class="control-lable">Topics</label>
										<select name="topicname" class="form-control"
											ng-model="selectedTopic.name" ng-change="countTotalQuestions()">
											<option value="" selected="selected">Select Topic</option>
											<option ng-repeat="topic in allTopics" value="{{topic}}">
												{{topic.name}}</option>
										</select>
									</div>
									<div class="col-md-3" ng-if="isTopic==true">
										<label class="form-label">Total Questions: {{totalQuestion}}</label>
									</div>
								</div>

								<div class="row">
									<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
										<li class="active"><a href="#green" data-toggle="tab">Write
												Question</a></li>
									</ul>

									<div id="my-tab-content" class="tab-content">
										<div class="tab-pane active" id="green" style="overflow:auto;height: 500px">
											<!-- <iframe src="http://odia.org/sw/webdhwani.php?lang=odia" style="width:100%;height:500px"></iframe> -->
											<div class="DivContent" id="divBody">
												<label class="control-label">Enter your question</label>
												<textarea id="TextAreaITran" class="form-control" style="font-size:20px"
													placeholder="Type your question here in english phonetic language..."></textarea>
												<textarea id="TextAreaOdia" ng-model="odia.question" class="form-control" style="font-size:20px"
													placeholder=""></textarea>
												<textarea id="html_text"></textarea>	
												<br>	
												<label class="control-label">Answer 1</label>
												<textarea id="option1Answer" class="form-control" style="font-size:20px"
													placeholder="Type  answer in english phonetic language..."></textarea>
												<textarea id="option1AnswerOdia" ng-model="odia.answer1" class="form-control" style="font-size:20px"
													placeholder=""></textarea>
													<br>
													<label class="control-label">Answer 2</label>
													<textarea id="option2Answer" class="form-control" style="font-size:20px"
													placeholder="Type  answer in english phonetic language..."></textarea> 
												<textarea id="option2AnswerOdia" ng-model="odia.answer2" class="form-control" style="font-size:20px"
													placeholder=""></textarea>
													<br>
													<label class="control-label">Answer 3</label>
													<textarea id="option3Answer" class="form-control" style="font-size:20px"
													placeholder="Type  answer in english phonetic language..."></textarea>
												<textarea id="option3AnswerOdia" ng-model="odia.answer3" class="form-control" style="font-size:20px"
													placeholder=""></textarea>
													<br>
													<label class="control-label">Answer 4</label>
													<textarea id="option4Answer" class="form-control" style="font-size:20px"
													placeholder="Type  answer in english phonetic language..."></textarea>
												<textarea id="option4AnswerOdia" ng-model="odia.answer4" class="form-control" style="font-size:20px"
													placeholder=""></textarea>	
													<br>
													<label class="control-label">Correct Answer</label>
													<input type="text" id="correctAnswer" ng-model="odia.correctAnswer" class="form-control" style="font-size:20px" name="correctAnswer"></input>
											</div>
										</div>
									</div>
									<a href class="btn btn-primary" ng-click="createQuestion()">Submit</a>
								</div>
							</div>
						</div>

						<!-- <div class="row">
						<h4 class="heading">Recent Exams</h4>
						<div class="box">
							<div class="box-gray ">Name: {{student.firstName}}
								{{student.lastName}} Board: {{student.board}} Class:
								{{student.standard}}</div>
						</div>
					</div>
					<div class="row">
						<h4 class="heading">Overall Report</h4>
						<div class="box">
							<div class="box-gray ">Name: {{student.firstName}}
								{{student.lastName}} Board: {{student.board}} Class:
								{{student.standard}}</div>
						</div>
					</div> -->
					</div>
				</div>

			</div>


		</div>

	</section>
</body>
</html>
