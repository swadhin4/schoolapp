
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page import="java.util.List"%>
<%@page import="com.jpa.entities.Question"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Home</title>

<script type="text/javascript">
	webContextPath = "${pageContext.request.contextPath}";

	$(document).ready(function() {
		/* 
		 $('#demo').jplist({
		 itemsBox : '.list',
		 itemPath : '.list-item',
		 panelPath : '.jplist-panel'
		 });
		 $("body").on("contextmenu",function(e){
		 return false;
		 });
		 $('body').live("copy",function(e) {
		 e.preventDefault();
		 });
		 */
		 var e = document.getElementById("refreshed");
		if (e.value == "no")
			e.value = "yes";
		else {
			e.value = "no";
			location.reload();
		} 

	});
</script>
<style>
body {
	-moz-user-select: none;
	-webkit-user-select: none;
	-ms-user-select: none;
	-o-user-select: none;
	user-select: none;
}

label.error {
	color: red;
}

.animate-enter,.animate-leave {
	-webkit-transition: 400ms cubic-bezier(0.250, 0.250, 0.750, 0.750) all;
	-moz-transition: 400ms cubic-bezier(0.250, 0.250, 0.750, 0.750) all;
	-ms-transition: 400ms cubic-bezier(0.250, 0.250, 0.750, 0.750) all;
	-o-transition: 400ms cubic-bezier(0.250, 0.250, 0.750, 0.750) all;
	transition: 400ms cubic-bezier(0.250, 0.250, 0.750, 0.750) all;
	position: relative;
	display: block;
}

.animate-enter.animate-enter-active,.animate-leave {
	opacity: 1;
	top: 0;
	height: 30px;
}

.animate-leave.animate-leave-active,.animate-enter {
	opacity: 0;
	top: -50px;
	height: 0px;
}

.open .btn-box-tool {
	color: #fff
}

.jplist-pagination-info {
	margin: 15px 15px 0 40px;
}

.jplist-items-per-page {
	margin: 15px 10px 0 0;
}

.jplist-pagination {
	margin: -18px -8px -17px 260px;
}

.center-block {
	width: 100%;
	margin: 20px auto;
	background: #fff;
	border: 1px solid #ddd;
	border-radius: 4px;
}

input[type=radio] {
	opacity: 0;
	position: absolute;
	z-index: 12;
	width: 30px;
	height: 30px;
}

input[type=radio]+.lbl::before {
	font-family: fontAwesome;
	font-weight: normal;
	font-size: 11px;
	color: #2091cf;
	content: "\a0";
	background-color: #FAFAFA;
	border: 1px solid #000;
	box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
	border-radius: 0;
	display: inline-block;
	text-align: center;
	vertical-align: middle;
	height: 13px;
	line-height: 13px;
	min-width: 13px;
	margin-right: 1px;
	margin-top: -5px;
}

input[type=radio]:checked+.lbl::before {
	display: inline-block;
	content: '\f00c';
	background-color: #F5F8FC;
	border-color: #adb8c0;
	box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px
		rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1);
}

input[type=radio]+.lbl::before {
	border-radius: 32px;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 36px;
}

input[type=radio]:checked+.lbl::before {
	content: "\2022";
}

div.countdown  span {
	font-size: 30px;
	font-weight: 300;
	line-height: 80px;
}
</style>

</head>
<%
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<body >
	<input type="hidden" id="refreshed" value="no">
	<c:set var="contextPath" value="${pageContext.request.contextPath}"></c:set>
	<section>

		<div class="container">
			<div class="tab-content">
				<div class="tab-pane fade in active">
					<div class="row">

						<div class="col-md-12">
							<form editable-form name="orderForm">
								<div class="box box-primary">
									<div class="box-header with-border">
										<h3 class="box-title" id="orderprocessid">Read the
											Instructions carefully</h3>
									</div>
									<div class="box-body">
										<div class="col-md-12">
											<div class="row">
												<div class="thumbnail" style="height: 435px">
													<div class="caption">
														<div class="box-header with-border">
															<h5 class="box-title" id="orderprocessid">Step 1</h5>
														</div>

													</div>
													<a href="${contextPath}/question/start"
														class="btn btn-primary" id="cd_start">Start</a>
												</div>
											</div>
										</div>
									</div>
								</div>

							</form>
						</div>

					</div>
				</div>
			</div>
		</div>

	</section>
</body>
</html>
