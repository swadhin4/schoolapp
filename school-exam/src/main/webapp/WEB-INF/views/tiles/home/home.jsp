<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false"%>
<html>
<head>
<title>Home</title>
<script
	src='<c:url value="/resources/js/angular-controller.js"></c:url>'></script>
	<script type="text/javascript">
$(document).ready(function() {
	
		 var e = document.getElementById("refreshed");
		if (e.value == "no")
			e.value = "yes";
		else {
			e.value = "no";
			location.reload();
		} 

	});
</script>
<style>
input[type=checkbox]
{
        opacity: 0;
        position: absolute;
        z-index: 12;
        width: 18px;
        height: 18px;
}

input[type=checkbox]+.lbl.padding-8::before{
        margin-right: 8px;
}


input[type=checkbox]+.lbl::before{
        font-family: fontAwesome;
        font-weight: normal;
        font-size: 11px;
        color: #2091cf;
        content: "\a0";
        background-color: #FAFAFA;
        border: 1px solid #CCC;
        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
        border-radius: 0;
        display: inline-block;
        text-align: center;
        vertical-align: middle;
        height: 13px;
        line-height: 13px;
        min-width: 13px;
        margin-right: 1px;
        margin-top: -5px;
}

input[type=checkbox]:checked+.lbl::before {
        display: inline-block;
        content: '\f00c';
        background-color: #F5F8FC;
        border-color: #adb8c0;
        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1);
}

a.list-group-item:hover, a.list-group-item:focus{
background-color: #5899b3;
color:#fff
}

</style>
</head>
<body ng-app="examApp">
<input type="hidden" id="refreshed" value="no">
	<c:set var="contextPath" value="${pageContext.request.contextPath}"></c:set>
	<section id="content">
		<div class="container" ng-controller="examCategoryController">
			<div class="row">
				<div class="col-lg-12">
					<div class="row">
						<div class="box">
							<div class="box-gray">
								<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
									<li><a href="#myprofile" data-toggle="tab">My Account</a></li>
									<li><a href="#orange" data-toggle="tab">Payment
											Information</a></li>
									<li class="active"><a href="#yellow" data-toggle="tab">Courses</a></li>
									<li><a href="#green" data-toggle="tab">Sample Test</a></li>
								</ul>

								<div id="my-tab-content" class="tab-content">
									<div class="tab-pane " id="myprofile">

										<div class="col-md-6">
											<h3>Account Snapshot</h3>
											<div class="box">
												<label class="control-label">Status:</label><br> <label
													class="control-label">Signup Date:
													{{student.createdDate}}</label><br> <label class="control-label">Account
													Balance: Rs. {{student.balance}}</label><br> <label class="control-label">Last
													Payment: </label>
											</div>



										</div>
										<div class="col-md-6">
											<h3>Account Details</h3>
											<div class="box">
												<label class="control-label">Name:
													{{student.firstName}} {{student.lastName}}</label> <label
													class="control-label">Class: {{student.standard}}</label><br>
												<label class="control-label">Board:
													{{student.board}}</label><br> <label class="control-label">Email:
													{{student.email}}</label><br> <label class="control-label">Mobile:
													{{student.mobile}}</label>
											</div>



										</div>
									</div>
									<div class="tab-pane" id="orange">
										<h1>Coming Soon</h1>
										<p>Under Development</p>
									</div>
									<div class="tab-pane active" id="yellow">

										<div class="list-group">
											<div class="col-md-3">
												<div ng-repeat="category in categories">
													<a href class="list-group-item btn-success" id="category{{$index}}"
														ng-click="getTopics(category[0])" style="color:#fff"><i
														class="fa fa-desktop"></i> {{category[1]}}</a>
												</div>
											</div>
											<div class="col-md-9">
											<div class="alert alert-danger alert-dismissible" role="alert" style="display:none" id="errorDiv">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											</div>
												<div ng-repeat="topic in topics" ng-show="subjectSelection==true">
													<div class="form-group">
														<div class="thumbnail">
															<div class="caption">
																<div class="box-header with-border">
																	<input type="checkbox" name="checkbox"
																 id="topic{{topic.id}}" value="{{topic.id}}"
																ng-click="selectTopic(topic.id)">
																<span class="lbl padding-8">{{topic.name}}</span>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div ng-show="subjectSelection==false">
													<div class="form-group">
														<div class="thumbnail">
															<div class="caption">
																Instructions:
																Click on the subject to view the topics.
																
															</div>
														</div>
													</div>
												</div>
												<div class="pull-right">
												<input type="button" class="btn btn-primary" id="validatebtn" ng-click="validateTopicSelection()" value="Next">
												<form action="${contextPath}/test/begin" method="post" name="testform" id="testform" style="display:none">
												    <input type="text" value="" id="subjectcode" name="subjectcode">
												    <input type="text" value="" id="queval" name="queval">
												     <input type="text" value="" id="topics" name="topics">
												      <input type="text" value="" id="maxQuestion" name="maxQuestion">
													<input type="submit" id="testPageId">Proceed</a>
												</form>		
												</div>
											</div>
										</div>

									</div>
									<div class="tab-pane" id="green">
										<h1>Test</h1>
										<p>In Progress</p>
									</div>
									<div class="tab-pane" id="blue">
										<h1>Blue</h1>
										<p>blue blue blue blue blue</p>
									</div>
								</div>
							</div>
						</div>

						<!-- <div class="row">
						<h4 class="heading">Recent Exams</h4>
						<div class="box">
							<div class="box-gray ">Name: {{student.firstName}}
								{{student.lastName}} Board: {{student.board}} Class:
								{{student.standard}}</div>
						</div>
					</div>
					<div class="row">
						<h4 class="heading">Overall Report</h4>
						<div class="box">
							<div class="box-gray ">Name: {{student.firstName}}
								{{student.lastName}} Board: {{student.board}} Class:
								{{student.standard}}</div>
						</div>
					</div> -->
					</div>
				</div>

			</div>
			<!-- Portfolio Projects -->
			<div class="row">
				<div class="col-lg-12">
					<div class="row">
						<div class="box">
							<div class="box-gray">
								<h4 class="heading">Recent Exams</h4>
								<div class="row">
									<section id="projects">
										<ul id="thumbs" class="portfolio">
											<!-- Item Project and Filter Name -->
											<li class="col-lg-3 design" data-id="id-0" data-type="web">
												<div class="item-thumbs">
													<!-- Fancybox - Gallery Enabled - Title - Full Image -->
													<a class="hover-wrap fancybox"
														data-fancybox-group="gallery" title="Work 1"
														href="img/works/1.jpg"> <span class="overlay-img"></span>
														<span class="overlay-img-thumb font-icon-plus"></span>
													</a>
													<!-- Thumb Image and Description -->
													<img src="img/works/1.jpg"
														alt="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis elementum odio. Curabitur pellentesque, dolor vel pharetra mollis.">
												</div>
											</li>
											<!-- End Item Project -->
											<!-- Item Project and Filter Name -->
											<li class="item-thumbs col-lg-3 design" data-id="id-1"
												data-type="icon">
												<!-- Fancybox - Gallery Enabled - Title - Full Image --> <a
												class="hover-wrap fancybox" data-fancybox-group="gallery"
												title="Work 2" href="img/works/2.jpg"> <span
													class="overlay-img"></span> <span
													class="overlay-img-thumb font-icon-plus"></span>
											</a> <!-- Thumb Image and Description --> <img
												src="img/works/2.jpg"
												alt="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis elementum odio. Curabitur pellentesque, dolor vel pharetra mollis.">
											</li>
											<!-- End Item Project -->
											<!-- Item Project and Filter Name -->
											<li class="item-thumbs col-lg-3 photography" data-id="id-2"
												data-type="illustrator">
												<!-- Fancybox - Gallery Enabled - Title - Full Image --> <a
												class="hover-wrap fancybox" data-fancybox-group="gallery"
												title="Work 3" href="img/works/3.jpg"> <span
													class="overlay-img"></span> <span
													class="overlay-img-thumb font-icon-plus"></span>
											</a> <!-- Thumb Image and Description --> <img
												src="img/works/3.jpg"
												alt="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis elementum odio. Curabitur pellentesque, dolor vel pharetra mollis.">
											</li>
											<!-- End Item Project -->
											<!-- Item Project and Filter Name -->
											<li class="item-thumbs col-lg-3 photography" data-id="id-2"
												data-type="illustrator">
												<!-- Fancybox - Gallery Enabled - Title - Full Image --> <a
												class="hover-wrap fancybox" data-fancybox-group="gallery"
												title="Work 4" href="img/works/4.jpg"> <span
													class="overlay-img"></span> <span
													class="overlay-img-thumb font-icon-plus"></span>
											</a> <!-- Thumb Image and Description --> <img
												src="img/works/4.jpg"
												alt="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis elementum odio. Curabitur pellentesque, dolor vel pharetra mollis.">
											</li>
											<!-- End Item Project -->
										</ul>
									</section>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</section>
</body>
</html>
