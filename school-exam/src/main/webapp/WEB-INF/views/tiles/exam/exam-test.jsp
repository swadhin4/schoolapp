<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false"%>
<html>
<head>
<title>Home</title>
<script
	src='<c:url value="/resources/js/questions-controller.js"></c:url>'></script>

</head>
<body ng-app="questionsApp">
	<c:set var="contextPath" value="${pageContext.request.contextPath}"></c:set>

	<section class="callaction">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<!-- <div class="big-cta">
					<div class="cta-text">
						<h2><span>Bookscue</span> Online Examination</h2>
					</div>
				</div> -->
				</div>
			</div>
		</div>
	</section>
	<section id="content">
		<div class="container" ng-controller="questionsController">
			<div class="row">
				<div class="col-lg-12">
					<div class="row">
						
					</div>
				</div>
				<!-- divider -->
				<div class="row">
					<div class="col-lg-12">
						<div class="solidline"></div>
					</div>
				</div>
			</div>
			</div>
	</section>
</body>
</html>
