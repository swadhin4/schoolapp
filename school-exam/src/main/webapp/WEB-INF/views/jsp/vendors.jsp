<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<div>
<c:choose>
	<c:when test="${empty vendors}"> 
								   No Vendors Available
								</c:when>
	<c:otherwise>
		<c:forEach items="${vendors}" var="vendor">
			<div class="row">
				<div class="col-sm-12">
					<div class="panel panel-default">
						<div class="panel-heading">${vendor.name }</div>
						<div class="panel-body">
						<c:forEach items="${vendor.gradeVOList}" var="grade">
						    ${grade.gradeName}
						  
						</c:forEach>
						</div>
					</div>
				</div>

			</div>
		</c:forEach>
	</c:otherwise>
</c:choose>
</div>


