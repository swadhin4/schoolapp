var testApp = angular.module("testApp", []);
var oTableAssum=null;
var selectedQuestions=[];
var selectedAnswers;
testApp.controller('testController', function($rootScope, $scope, $http,
		$filter, $q) {

	$('#submitbtn').attr('disabled',false);
	$('.questionradios:checked').each(function(){
		$(this).attr('disabled',false);
		$(this).attr('checked',false);
	})
	$scope.userAnswers;
	var answeredValues=[];
	$scope.submitTest=function(name, subjectid){
		
		$('.questionlist li input[type=radio]:checked').each(function(){
			console.log($(this).val());
			answeredValues.push($(this).val())
		})
		console.log(JSON.stringify(answeredValues));
		
		if(answeredValues.length>0){
						$('.questionradios:checked').each(function(){
				$(this).attr('disabled',true);
			})
			$scope.userAnswers={
				 subject:{
					  subjectid:subjectid,
					  name:$('#subjectname').text(),
					  answers:answeredValues
				 },
				 questionDone:answeredValues.length,
			}
			console.log(JSON.stringify($scope.userAnswers))
			$('#confirmBtn').click();
		}else{
			
		}
	}
	

});

function selectedOption(answer,selquestionid){
	var scope = angular.element('#testCtrlId').scope();
	var answerSelected=answer.value;
	var questionNum=answerSelected.split(",");
	//alert(questionid+"-"+questionNum[1]);
	$('#q'+questionNum[1]).css("background-color","#428bca");
	$('#q'+questionNum[1]).css("color","#fff");
	$('#s'+questionNum[1]).html('<i class="fa fa-cog"></i>');
	selectedAnswers={};
	$('input[name=radio'+selquestionid+']').each(function(){
				selectedAnswers={
					question:questionNum[0],
					value:questionNum[2],
					isSelected:$(this).is(":checked"),
					correctAns:$('#ans'+questionNum[0]).val()
				
				}
	});
	
	$.ajax({
		url:'/result/store',
		type:"get",
		data:{questionValues: JSON.stringify(selectedAnswers)},
		success:function(data){
			console.log(data);
		}
	})	
	
	
	
}