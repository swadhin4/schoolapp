var examApp=angular.module("examApp", []);

examApp.controller('examCategoryController',  function($rootScope,$scope,$http,$filter,$q) {
	
	$scope.categories=[];
	
	/*$http.get('/app/resources/json/categories.json').
	success(function(data) {
		$scope.categories=data;
	});*/
	
	$scope.student=null;
	
	$http.get('/home/student/details').
	success(function(data) {
		console.log(data);
		$scope.student=data;
		
		$scope.getSubjects(data.board);
	});
	
	$scope.getSubjects=function(board){
		$http.get('/board/subjects/'+board).
		success(function(data) {
			$scope.categories=data;
		});
		$('#category0').css('background-color','#4088b6');
		$('#category0').css('color','#fff');
	}
	
	$scope.selectedSubject;
	$scope.subjectSelection=false;
	$scope.getTopics=function(subjectid){
		$scope.topics;
		$http.get('/board/subjects/topics/'+subjectid).
		success(function(data) {
			$scope.subjectSelection=true;
			console.log(JSON.stringify(data));
			$scope.topics=data.object.sessionValues.topics
			$scope.selectedSubject={
					sessioncode:data.object.sessionSubject,
					queval:data.object.subjectid,
					maxQuestion:data.object.sessionValues.maxQuestion
			}
		});
	}
	
	
	
	
	$('input[type=checkbox]').each(function() { this.checked = false; }); 
	
	$scope.subjectTopics = [];
	$scope.topicQuestions=[];
	$scope.selectTopic = function(topicSelected) {
		var index=$.inArray(topicSelected,$scope.subjectTopics);
		if(index === -1){
			$scope.subjectTopics.push(topicSelected);
			
		}else{
			$scope.subjectTopics.splice(index,1);
		}
		
	}
	
	$scope.validateTopicSelection=function(){
		if($scope.selectedSubject == undefined){
			$('#errorDiv').show();
			$('#errorDiv').html("<b>Please ensure that a subject is selected.</b>");
		}
		else if($scope.subjectTopics.length==0){
			$('#errorDiv').show();
			$('#errorDiv').html("<b>Please ensure at least a topic is selected for subject.</b>");
		}else{
			console.log("$scope.subjectTopics : "  + JSON.stringify($scope.subjectTopics));
			$('#subjectcode').val($scope.selectedSubject.sessioncode);
			$('#queval').val($scope.selectedSubject.queval);
			$('#topics').val($scope.subjectTopics);
			$('#maxQuestion').val($scope.selectedSubject.maxQuestion);
			$('#testPageId').click();
				/*$http.get('/app/question/topic/'+$scope.subjectTopics+'/'+$scope.selectedSubject.maxQuestion)
				.success(function(data){
					if(data.statusCode==200){
						
						//window.location.href="/app/test/begin?subjectcode="+$scope.selectedSubject.sessioncode+"&queval="+$scope.selectedSubject.queval;
					}else{
						
					}
				});*/
		
		}
	}



});

