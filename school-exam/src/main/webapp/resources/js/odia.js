//Created using vb_to_js_assoc_array.pl 
var Oriya_kmap = {
// Oriya kmap according to Itrans transliteration
// by Miikka-Markus Alhonen 2002-01-26
// changed by www.odia.org Jan-2008

//*******************************
// Independent vowels
//*******************************

'a':'\u0b05',
'A':'\u0b06',
'aa':'\u0b06',
'i':'\u0b07',
'I':'\u0b08',
'ii':'\u0b08',
'u':'\u0b09',
'U':'\u0b0a',
'uu':'\u0b0a',
//Harsha Ru
'R':'\u0b0b',
'R^':'\u0b0b',
'R^i':'\u0b0b',

//Dirgha Ru
'RR':'\u0b60',
'RRi':'\u0b60',
'R^I':'\u0b60',
'RRI':'\u0b60',

'L^':'\u0b0c',
'L^i':'\u0b0c',
'LL':'\u0b0c',
'LLi':'\u0b0c',
'L^I':'\u0b61',
'LLI':'\u0b61',

'e':'\u0b0f',
'ai':'\u0b10',
'o':'\u0b13',
'au':'\u0b14',



// Numbers

//0x30=0x0b66
//0x31=0x0b67
//0x32=0x0b68
//0x33=0x0b69
//0x34=0x0b6a
//0x35=0x0b6b
//0x36=0x0b6c
//0x37=0x0b6d
//0x38=0x0b6e
//0x39=0x0b6f

'0':'\u0b66',
'1':'\u0b67',
'2':'\u0b68',
'3':'\u0b69',
'4':'\u0b6a',
'5':'\u0b6b',
'6':'\u0b6c',
'7':'\u0b6d',
'8':'\u0b6e',
'9':'\u0b6f',

// Other marks

'\\':'\u200c',
'\'':'\u030d',
'\"':'\u030e',
'\'\'':'\u030e',
'\`':'\u0331',
'\_':'\u0331',

'.N':'\u0b01',
'.n':'\u0b02',
'M':'\u0b02',
'H':'\u0b03',
'.a':'\u0b3d',
//.h=0x0b4d 0x200c
'.h':'\u200c',
'.':'\u002e',
//.=0x0964
//..=0x0965
//|=0x0964
//||=0x0965
//;=0x0965
//{\\rm 0x20 .}=0x002e

//ZERO WIDTH JOINER' (U+200D) 
'{':'\u007b',
'{}':'\u200c',

//**************************
// Consonant + vowel/virama
//****************************

//ka barga
'k':'\u0b15\u0b4d',
'k.h':'\u0b15\u0b4d\u200c',
'kA':'\u0b15\u0b3e',
'kI':'\u0b15\u0b40',
'kR':'\u0b15\u0b43',
'kRRi':'\u0b15\u0b43',
'kR^i':'\u0b15\u0b43',
'kU':'\u0b15\u0b42',
'ka':'\u0b15',
'kaa':'\u0b15\u0b3e',
'kai':'\u0b15\u0b48',
'kau':'\u0b15\u0b4c',
'ke':'\u0b15\u0b47',
'ki':'\u0b15\u0b3f',
'kii':'\u0b15\u0b40',
'ko':'\u0b15\u0b4b',
'ku':'\u0b15\u0b41',
'kuu':'\u0b15\u0b42',

'kh':'\u0b16\u0b4d',
'kh.h':'\u0b16\u0b4d\u200c',
'khA':'\u0b16\u0b3e',
'khI':'\u0b16\u0b40',
'khR':'\u0b16\u0b43',
'khRRi':'\u0b16\u0b43',
'khR^i':'\u0b16\u0b43',
'khU':'\u0b16\u0b42',
'kha':'\u0b16',
'khaa':'\u0b16\u0b3e',
'khai':'\u0b16\u0b48',
'khau':'\u0b16\u0b4c',
'khe':'\u0b16\u0b47',
'khi':'\u0b16\u0b3f',
'khii':'\u0b16\u0b40',
'kho':'\u0b16\u0b4b',
'khu':'\u0b16\u0b41',
'khuu':'\u0b16\u0b42',

'g':'\u0b17\u0b4d',
'g.h':'\u0b17\u0b4d\u200c',
'gA':'\u0b17\u0b3e',
'gI':'\u0b17\u0b40',
'gR':'\u0b17\u0b43',
'gRRi':'\u0b17\u0b43',
'gR^i':'\u0b17\u0b43',
'gU':'\u0b17\u0b42',
'ga':'\u0b17',
'gaa':'\u0b17\u0b3e',
'gai':'\u0b17\u0b48',
'gau':'\u0b17\u0b4c',
'ge':'\u0b17\u0b47',
'gi':'\u0b17\u0b3f',
'gii':'\u0b17\u0b40',
'go':'\u0b17\u0b4b',
'gu':'\u0b17\u0b41',
'guu':'\u0b17\u0b42',
'gh':'\u0b18\u0b4d',
'gh.h':'\u0b18\u0b4d\u200c',
'ghA':'\u0b18\u0b3e',
'ghI':'\u0b18\u0b40',
'ghR':'\u0b18\u0b43',
'ghRRi':'\u0b18\u0b43',
'ghR^i':'\u0b18\u0b43',
'ghU':'\u0b18\u0b42',
'gha':'\u0b18',
'ghaa':'\u0b18\u0b3e',
'ghai':'\u0b18\u0b48',
'ghau':'\u0b18\u0b4c',
'ghe':'\u0b18\u0b47',
'ghi':'\u0b18\u0b3f',
'ghii':'\u0b18\u0b40',
'gho':'\u0b18\u0b4b',
'ghu':'\u0b18\u0b41',
'ghuu':'\u0b18\u0b42',

//Gaga added:- ~ (tilda) is added to suit the logic. it is defaulted to ~N
'~':'\u0b19\u0b4d',
//End of Gaga addition

'~N':'\u0b19\u0b4d',
'~N.h':'\u0b19\u0b4d\u200c',
'~NA':'\u0b19\u0b3e',
'~NI':'\u0b19\u0b40',
'~NR':'\u0b19\u0b43',
'~NRRi':'\u0b19\u0b43',
'~NR^i':'\u0b19\u0b43',
'~NU':'\u0b19\u0b42',
'~Na':'\u0b19',
'~Naa':'\u0b19\u0b3e',
'~Nai':'\u0b19\u0b48',
'~Nau':'\u0b19\u0b4c',
'~Ne':'\u0b19\u0b47',
'~Ni':'\u0b19\u0b3f',
'~Nii':'\u0b19\u0b40',
'~No':'\u0b19\u0b4b',
'~Nu':'\u0b19\u0b41',
'~Nuu':'\u0b19\u0b42',

//cha barga

'c':'\u0b1a\u0b4d',
'c.h':'\u0b1a\u0b4d\u200c',
'cA':'\u0b1a\u0b3e',
'cI':'\u0b1a\u0b40',
'cR':'\u0b1a\u0b43',
'cRRi':'\u0b1a\u0b43',
'cR^i':'\u0b1a\u0b43',
'cU':'\u0b1a\u0b42',
'ca':'\u0b1a',
'caa':'\u0b1a\u0b3e',
'cai':'\u0b1a\u0b48',
'cau':'\u0b1a\u0b4c',
'ce':'\u0b1a\u0b47',
'ci':'\u0b1a\u0b3f',
'cii':'\u0b1a\u0b40',
'co':'\u0b1a\u0b4b',
'cu':'\u0b1a\u0b41',
'cuu':'\u0b1a\u0b42',
'ch':'\u0b1a\u0b4d',
'ch.h':'\u0b1a\u0b4d\u200c',
'chA':'\u0b1a\u0b3e',
'chI':'\u0b1a\u0b40',
'chR':'\u0b1a\u0b43',
'chRRi':'\u0b1a\u0b43',
'chR^i':'\u0b1a\u0b43',
'chU':'\u0b1a\u0b42',
'cha':'\u0b1a',
'chaa':'\u0b1a\u0b3e',
'chai':'\u0b1a\u0b48',
'chau':'\u0b1a\u0b4c',
'che':'\u0b1a\u0b47',
'chi':'\u0b1a\u0b3f',
'chii':'\u0b1a\u0b40',
'cho':'\u0b1a\u0b4b',
'chu':'\u0b1a\u0b41',
'chuu':'\u0b1a\u0b42',
'chh':'\u0b1b\u0b4d',
'chh.h':'\u0b1b\u0b4d\u200c',
'chhA':'\u0b1b\u0b3e',
'chhI':'\u0b1b\u0b40',
'chhR':'\u0b1b\u0b43',
'chhRRi':'\u0b1b\u0b43',
'chhR^i':'\u0b1b\u0b43',
'chhU':'\u0b1b\u0b42',
'chha':'\u0b1b',
'chhaa':'\u0b1b\u0b3e',
'chhai':'\u0b1b\u0b48',
'chhau':'\u0b1b\u0b4c',
'chhe':'\u0b1b\u0b47',
'chhi':'\u0b1b\u0b3f',
'chhii':'\u0b1b\u0b40',
'chho':'\u0b1b\u0b4b',
'chhu':'\u0b1b\u0b41',
'chhuu':'\u0b1b\u0b42',

'Ch':'\u0b1b\u0b4d',
'Ch.h':'\u0b1b\u0b4d\u200c',
'ChA':'\u0b1b\u0b3e',
'ChI':'\u0b1b\u0b40',
'ChR':'\u0b1b\u0b43',
'ChRRi':'\u0b1b\u0b43',
'ChR^i':'\u0b1b\u0b43',
'ChU':'\u0b1b\u0b42',
'Cha':'\u0b1b',
'Chaa':'\u0b1b\u0b3e',
'Chai':'\u0b1b\u0b48',
'Chau':'\u0b1b\u0b4c',
'Che':'\u0b1b\u0b47',
'Chi':'\u0b1b\u0b3f',
'Chii':'\u0b1b\u0b40',
'Cho':'\u0b1b\u0b4b',
'Chu':'\u0b1b\u0b41',
'Chuu':'\u0b1b\u0b42',

'j':'\u0b1c\u0b4d',
'j.h':'\u0b1c\u0b4d\u200c',
'jA':'\u0b1c\u0b3e',
'jI':'\u0b1c\u0b40',
'jR':'\u0b1c\u0b43',
'jRRi':'\u0b1c\u0b43',
'jR^i':'\u0b1c\u0b43',
'jU':'\u0b1c\u0b42',
'ja':'\u0b1c',
'jaa':'\u0b1c\u0b3e',
'jai':'\u0b1c\u0b48',
'jau':'\u0b1c\u0b4c',
'je':'\u0b1c\u0b47',
'ji':'\u0b1c\u0b3f',
'jii':'\u0b1c\u0b40',
'jo':'\u0b1c\u0b4b',
'ju':'\u0b1c\u0b41',
'juu':'\u0b1c\u0b42',
'jh':'\u0b1d\u0b4d',
'jh.h':'\u0b1d\u0b4d\u200c',
'jhA':'\u0b1d\u0b3e',
'jhI':'\u0b1d\u0b40',
'jhR':'\u0b1d\u0b43',
'jhRRi':'\u0b1d\u0b43',
'jhR^i':'\u0b1d\u0b43',
'jhU':'\u0b1d\u0b42',
'jha':'\u0b1d',
'jhaa':'\u0b1d\u0b3e',
'jhai':'\u0b1d\u0b48',
'jhau':'\u0b1d\u0b4c',
'jhe':'\u0b1d\u0b47',
'jhi':'\u0b1d\u0b3f',
'jhii':'\u0b1d\u0b40',
'jho':'\u0b1d\u0b4b',
'jhu':'\u0b1d\u0b41',
'jhuu':'\u0b1d\u0b42',

'~n':'\u0b1e\u0b4d',
'~n.h':'\u0b1e\u0b4d\u200c',
'~nA':'\u0b1e\u0b3e',
'~nI':'\u0b1e\u0b40',
'~nR':'\u0b1e\u0b43',
'~nRRi':'\u0b1e\u0b43',
'~nR^i':'\u0b1e\u0b43',
'~nU':'\u0b1e\u0b42',
'~na':'\u0b1e',
'~naa':'\u0b1e\u0b3e',
'~nai':'\u0b1e\u0b48',
'~nau':'\u0b1e\u0b4c',
'~ne':'\u0b1e\u0b47',
'~ni':'\u0b1e\u0b3f',
'~nii':'\u0b1e\u0b40',
'~no':'\u0b1e\u0b4b',
'~nu':'\u0b1e\u0b41',
'~nuu':'\u0b1e\u0b42',

'~nJ':'\u0b1e\u0b1d\u0b4d',

//Ta barga

'T':'\u0b1f\u0b4d',
'T.h':'\u0b1f\u0b4d\u200c',
'TA':'\u0b1f\u0b3e',
'TI':'\u0b1f\u0b40',
'TR':'\u0b1f\u0b43',
'TRRi':'\u0b1f\u0b43',
'TR^i':'\u0b1f\u0b43',
'TU':'\u0b1f\u0b42',
'Ta':'\u0b1f',
'Taa':'\u0b1f\u0b3e',
'Tai':'\u0b1f\u0b48',
'Tau':'\u0b1f\u0b4c',
'Te':'\u0b1f\u0b47',
'Ti':'\u0b1f\u0b3f',
'Tii':'\u0b1f\u0b40',
'To':'\u0b1f\u0b4b',
'Tu':'\u0b1f\u0b41',
'Tuu':'\u0b1f\u0b42',
'Th':'\u0b20\u0b4d',
'Th.h':'\u0b20\u0b4d\u200c',
'ThA':'\u0b20\u0b3e',
'ThI':'\u0b20\u0b40',
'ThR':'\u0b20\u0b43',
'ThRRi':'\u0b20\u0b43',
'ThR^i':'\u0b20\u0b43',
'ThU':'\u0b20\u0b42',
'Tha':'\u0b20',
'Thaa':'\u0b20\u0b3e',
'Thai':'\u0b20\u0b48',
'Thau':'\u0b20\u0b4c',
'The':'\u0b20\u0b47',
'Thi':'\u0b20\u0b3f',
'Thii':'\u0b20\u0b40',
'Tho':'\u0b20\u0b4b',
'Thu':'\u0b20\u0b41',
'Thuu':'\u0b20\u0b42',


'D':'\u0b21\u0b4d',
'D.h':'\u0b21\u0b4d\u200c',
'DA':'\u0b21\u0b3e',
'DI':'\u0b21\u0b40',
'DR':'\u0b21\u0b43',
'DRRi':'\u0b21\u0b43',
'DR^i':'\u0b21\u0b43',
'DU':'\u0b21\u0b42',
'Da':'\u0b21',
'Daa':'\u0b21\u0b3e',
'Dai':'\u0b21\u0b48',
'Dau':'\u0b21\u0b4c',
'De':'\u0b21\u0b47',
'Di':'\u0b21\u0b3f',
'Dii':'\u0b21\u0b40',
'Do':'\u0b21\u0b4b',
'Du':'\u0b21\u0b41',
'Duu':'\u0b21\u0b42',
'Dh':'\u0b22\u0b4d',
'Dh.h':'\u0b22\u0b4d\u200c',
'DhA':'\u0b22\u0b3e',
'DhI':'\u0b22\u0b40',
'DhR':'\u0b22\u0b43',
'DhRRi':'\u0b22\u0b43',
'DhR^i':'\u0b22\u0b43',
'DhU':'\u0b22\u0b42',
'Dha':'\u0b22',
'Dhaa':'\u0b22\u0b3e',
'Dhai':'\u0b22\u0b48',
'Dhau':'\u0b22\u0b4c',
'Dhe':'\u0b22\u0b47',
'Dhi':'\u0b22\u0b3f',
'Dhii':'\u0b22\u0b40',
'Dho':'\u0b22\u0b4b',
'Dhu':'\u0b22\u0b41',
'Dhuu':'\u0b22\u0b42',

'.D':'\u0b5c\u0b4d',
'.D.h':'\u0b5c\u0b4d\u200c',
'.DA':'\u0b5c\u0b3e',
'.DI':'\u0b5c\u0b40',
'.DR':'\u0b5c\u0b43',
'.DRRi':'\u0b5c\u0b43',
'.DR^i':'\u0b5c\u0b43',
'.DU':'\u0b5c\u0b42',
'.Da':'\u0b5c',
'.Daa':'\u0b5c\u0b3e',
'.Dai':'\u0b5c\u0b48',
'.Dau':'\u0b5c\u0b4c',
'.De':'\u0b5c\u0b47',
'.Di':'\u0b5c\u0b3f',
'.Dii':'\u0b5c\u0b40',
'.Do':'\u0b5c\u0b4b',
'.Du':'\u0b5c\u0b41',
'.Duu':'\u0b5c\u0b42',
'.Dh':'\u0b5d\u0b4d',
'.Dh.h':'\u0b5d\u0b4d\u200c',
'.DhA':'\u0b5d\u0b3e',
'.DhI':'\u0b5d\u0b40',
'.DhR':'\u0b5d\u0b43',
'.DhRRi':'\u0b5d\u0b43',
'.DhR^i':'\u0b5d\u0b43',
'.DhU':'\u0b5d\u0b42',
'.Dha':'\u0b5d',
'.Dhaa':'\u0b5d\u0b3e',
'.Dhai':'\u0b5d\u0b48',
'.Dhau':'\u0b5d\u0b4c',
'.Dhe':'\u0b5d\u0b47',
'.Dhi':'\u0b5d\u0b3f',
'.Dhii':'\u0b5d\u0b40',
'.Dho':'\u0b5d\u0b4b',
'.Dhu':'\u0b5d\u0b41',
'.Dhuu':'\u0b5d\u0b42',

'N':'\u0b23\u0b4d',
'N.h':'\u0b23\u0b4d\u200c',
'NA':'\u0b23\u0b3e',
'NI':'\u0b23\u0b40',
'NR':'\u0b23\u0b43',
'NRRi':'\u0b23\u0b43',
'NR^i':'\u0b23\u0b43',
'NU':'\u0b23\u0b42',
'Na':'\u0b23',
'Naa':'\u0b23\u0b3e',
'Nai':'\u0b23\u0b48',
'Nau':'\u0b23\u0b4c',
'Ne':'\u0b23\u0b47',
'Ni':'\u0b23\u0b3f',
'Nii':'\u0b23\u0b40',
'No':'\u0b23\u0b4b',
'Nu':'\u0b23\u0b41',
'Nuu':'\u0b23\u0b42',
'N^':'\u0b19\u0b4d',
'N^.h':'\u0b19\u0b4d\u200c',
'N^A':'\u0b19\u0b3e',
'N^I':'\u0b19\u0b40',
'N^R':'\u0b19\u0b43',
'N^RRi':'\u0b19\u0b43',
'N^R^i':'\u0b19\u0b43',
'N^U':'\u0b19\u0b42',
'N^a':'\u0b19',
'N^aa':'\u0b19\u0b3e',
'N^ai':'\u0b19\u0b48',
'N^au':'\u0b19\u0b4c',
'N^e':'\u0b19\u0b47',
'N^i':'\u0b19\u0b3f',
'N^ii':'\u0b19\u0b40',
'N^o':'\u0b19\u0b4b',
'N^u':'\u0b19\u0b41',
'N^uu':'\u0b19\u0b42',

//ta barga
't':'\u0b24\u0b4d',
't.h':'\u0b24\u0b4d\u200c',
'tA':'\u0b24\u0b3e',
'tI':'\u0b24\u0b40',
'tR':'\u0b24\u0b43',
'tRRi':'\u0b24\u0b43',
'tR^i':'\u0b24\u0b43',
'tU':'\u0b24\u0b42',
'ta':'\u0b24',
'taa':'\u0b24\u0b3e',
'tai':'\u0b24\u0b48',
'tau':'\u0b24\u0b4c',
'te':'\u0b24\u0b47',
'ti':'\u0b24\u0b3f',
'tii':'\u0b24\u0b40',
'to':'\u0b24\u0b4b',
'tu':'\u0b24\u0b41',
'tuu':'\u0b24\u0b42',
'th':'\u0b25\u0b4d',
'th.h':'\u0b25\u0b4d\u200c',
'thA':'\u0b25\u0b3e',
'thI':'\u0b25\u0b40',
'thR':'\u0b25\u0b43',
'thRRi':'\u0b25\u0b43',
'thR^i':'\u0b25\u0b43',
'thU':'\u0b25\u0b42',
'tha':'\u0b25',
'thaa':'\u0b25\u0b3e',
'thai':'\u0b25\u0b48',
'thau':'\u0b25\u0b4c',
'the':'\u0b25\u0b47',
'thi':'\u0b25\u0b3f',
'thii':'\u0b25\u0b40',
'tho':'\u0b25\u0b4b',
'thu':'\u0b25\u0b41',
'thuu':'\u0b25\u0b42',

'd':'\u0b26\u0b4d',
'd.h':'\u0b26\u0b4d\u200c',
'dA':'\u0b26\u0b3e',
'dI':'\u0b26\u0b40',
'dR':'\u0b26\u0b43',
'dRRi':'\u0b26\u0b43',
'dR^i':'\u0b26\u0b43',
'dU':'\u0b26\u0b42',
'da':'\u0b26',
'daa':'\u0b26\u0b3e',
'dai':'\u0b26\u0b48',
'dau':'\u0b26\u0b4c',
'de':'\u0b26\u0b47',
'di':'\u0b26\u0b3f',
'dii':'\u0b26\u0b40',
'do':'\u0b26\u0b4b',
'du':'\u0b26\u0b41',
'duu':'\u0b26\u0b42',
'dh':'\u0b27\u0b4d',
'dh.h':'\u0b27\u0b4d\u200c',
'dhA':'\u0b27\u0b3e',
'dhI':'\u0b27\u0b40',
'dhR':'\u0b27\u0b43',
'dhRRi':'\u0b27\u0b43',
'dhR^i':'\u0b27\u0b43',
'dhU':'\u0b27\u0b42',
'dha':'\u0b27',
'dhaa':'\u0b27\u0b3e',
'dhai':'\u0b27\u0b48',
'dhau':'\u0b27\u0b4c',
'dhe':'\u0b27\u0b47',
'dhi':'\u0b27\u0b3f',
'dhii':'\u0b27\u0b40',
'dho':'\u0b27\u0b4b',
'dhu':'\u0b27\u0b41',
'dhuu':'\u0b27\u0b42',
'dny':'\u0b1c\u0b4d\u0b1e\u0b4d',
'dny.h':'\u0b1c\u0b4d\u0b1e\u0b4d\u200c',
'dnyA':'\u0b1c\u0b4d\u0b1e\u0b3e',
'dnyI':'\u0b1c\u0b4d\u0b1e\u0b40',
'dnyR':'\u0b1c\u0b4d\u0b1e\u0b43',
'dnyRRi':'\u0b1c\u0b4d\u0b1e\u0b43',
'dnyR^i':'\u0b1c\u0b4d\u0b1e\u0b43',
'dnyU':'\u0b1c\u0b4d\u0b1e\u0b42',
'dnya':'\u0b1c\u0b4d\u0b1e',
'dnyaa':'\u0b1c\u0b4d\u0b1e\u0b3e',
'dnyai':'\u0b1c\u0b4d\u0b1e\u0b48',
'dnyau':'\u0b1c\u0b4d\u0b1e\u0b4c',
'dnye':'\u0b1c\u0b4d\u0b1e\u0b47',
'dnyi':'\u0b1c\u0b4d\u0b1e\u0b3f',
'dnyii':'\u0b1c\u0b4d\u0b1e\u0b40',
'dnyo':'\u0b1c\u0b4d\u0b1e\u0b4b',
'dnyu':'\u0b1c\u0b4d\u0b1e\u0b41',
'dnyuu':'\u0b1c\u0b4d\u0b1e\u0b42',
'n':'\u0b28\u0b4d',
'n.h':'\u0b28\u0b4d\u200c',
'nA':'\u0b28\u0b3e',
'nI':'\u0b28\u0b40',
'nR':'\u0b28\u0b43',
'nRRi':'\u0b28\u0b43',
'nR^i':'\u0b28\u0b43',
'nU':'\u0b28\u0b42',
'na':'\u0b28',
'naa':'\u0b28\u0b3e',
'nai':'\u0b28\u0b48',
'nau':'\u0b28\u0b4c',
'ne':'\u0b28\u0b47',
'ni':'\u0b28\u0b3f',
'nii':'\u0b28\u0b40',
'no':'\u0b28\u0b4b',
'nu':'\u0b28\u0b41',
'nuu':'\u0b28\u0b42',

//pa barga

'p':'\u0b2a\u0b4d',
'p.h':'\u0b2a\u0b4d\u200c',
'pA':'\u0b2a\u0b3e',
'pI':'\u0b2a\u0b40',
'pR':'\u0b2a\u0b43',
'pRRi':'\u0b2a\u0b43',
'pR^i':'\u0b2a\u0b43',
'pU':'\u0b2a\u0b42',
'pa':'\u0b2a',
'paa':'\u0b2a\u0b3e',
'pai':'\u0b2a\u0b48',
'pau':'\u0b2a\u0b4c',
'pe':'\u0b2a\u0b47',
'pi':'\u0b2a\u0b3f',
'pii':'\u0b2a\u0b40',
'po':'\u0b2a\u0b4b',
'pu':'\u0b2a\u0b41',
'puu':'\u0b2a\u0b42',
'ph':'\u0b2b\u0b4d',
'ph.h':'\u0b2b\u0b4d\u200c',
'phA':'\u0b2b\u0b3e',
'phI':'\u0b2b\u0b40',
'phR':'\u0b2b\u0b43',
'phRRi':'\u0b2b\u0b43',
'phR^i':'\u0b2b\u0b43',
'phU':'\u0b2b\u0b42',
'pha':'\u0b2b',
'phaa':'\u0b2b\u0b3e',
'phai':'\u0b2b\u0b48',
'phau':'\u0b2b\u0b4c',
'phe':'\u0b2b\u0b47',
'phi':'\u0b2b\u0b3f',
'phii':'\u0b2b\u0b40',
'pho':'\u0b2b\u0b4b',
'phu':'\u0b2b\u0b41',
'phuu':'\u0b2b\u0b42',


'b':'\u0b2c\u0b4d',
'b.h':'\u0b2c\u0b4d\u200c',
'bA':'\u0b2c\u0b3e',
'bI':'\u0b2c\u0b40',
'bR':'\u0b2c\u0b43',
'bRRi':'\u0b2c\u0b43',
'bR^i':'\u0b2c\u0b43',
'bU':'\u0b2c\u0b42',
'ba':'\u0b2c',
'baa':'\u0b2c\u0b3e',
'bai':'\u0b2c\u0b48',
'bau':'\u0b2c\u0b4c',
'be':'\u0b2c\u0b47',
'bi':'\u0b2c\u0b3f',
'bii':'\u0b2c\u0b40',
'bo':'\u0b2c\u0b4b',
'bu':'\u0b2c\u0b41',
'buu':'\u0b2c\u0b42',
'bh':'\u0b2d\u0b4d',
'bh.h':'\u0b2d\u0b4d\u200c',
'bhA':'\u0b2d\u0b3e',
'bhI':'\u0b2d\u0b40',
'bhR':'\u0b2d\u0b43',
'bhRRi':'\u0b2d\u0b43',
'bhR^i':'\u0b2d\u0b43',
'bhU':'\u0b2d\u0b42',
'bha':'\u0b2d',
'bhaa':'\u0b2d\u0b3e',
'bhai':'\u0b2d\u0b48',
'bhau':'\u0b2d\u0b4c',
'bhe':'\u0b2d\u0b47',
'bhi':'\u0b2d\u0b3f',
'bhii':'\u0b2d\u0b40',
'bho':'\u0b2d\u0b4b',
'bhu':'\u0b2d\u0b41',
'bhuu':'\u0b2d\u0b42',

'm':'\u0b2e\u0b4d',
'm.h':'\u0b2e\u0b4d\u200c',
'mA':'\u0b2e\u0b3e',
'mI':'\u0b2e\u0b40',
'mR':'\u0b2e\u0b43',
'mRRi':'\u0b2e\u0b43',
'mR^i':'\u0b2e\u0b43',
'mU':'\u0b2e\u0b42',
'ma':'\u0b2e',
'maa':'\u0b2e\u0b3e',
'mai':'\u0b2e\u0b48',
'mau':'\u0b2e\u0b4c',
'me':'\u0b2e\u0b47',
'mi':'\u0b2e\u0b3f',
'mii':'\u0b2e\u0b40',
'mo':'\u0b2e\u0b4b',
'mu':'\u0b2e\u0b41',
'muu':'\u0b2e\u0b42',

//ya ra la sa sha shha ha kshh


//Gaga changed Y to y
'y':'\u0b5f\u0b4d',
'y.h':'\u0b5f\u0b4d\u200c',
'yA':'\u0b5f\u0b3e',
'yI':'\u0b5f\u0b40',
'yR':'\u0b5f\u0b43',
'yRRi':'\u0b5f\u0b43',
'yR^i':'\u0b5f\u0b43',
'yU':'\u0b5f\u0b42',
'ya':'\u0b5f',
'yaa':'\u0b5f\u0b3e',
'yai':'\u0b5f\u0b48',
'yau':'\u0b5f\u0b4c',
'ye':'\u0b5f\u0b47',
'yi':'\u0b5f\u0b3f',
'yii':'\u0b5f\u0b40',
'yo':'\u0b5f\u0b4b',
'yu':'\u0b5f\u0b41',
'yuu':'\u0b5f\u0b42',


//Gaga changed y to Y
'Y':'\u0b2f\u0b4d',
'Y.h':'\u0b2f\u0b4d\u200c',
'YA':'\u0b2f\u0b3e',
'YI':'\u0b2f\u0b40',
'YR':'\u0b2f\u0b43',
'YRRi':'\u0b2f\u0b43',
'YR^i':'\u0b2f\u0b43',
'YU':'\u0b2f\u0b42',
'Ya':'\u0b2f',
'Yaa':'\u0b2f\u0b3e',
'Yai':'\u0b2f\u0b48',
'Yau':'\u0b2f\u0b4c',
'Ye':'\u0b2f\u0b47',
'Yi':'\u0b2f\u0b3f',
'Yii':'\u0b2f\u0b40',
'Yo':'\u0b2f\u0b4b',
'Yu':'\u0b2f\u0b41',
'Yuu':'\u0b2f\u0b42',


'r':'\u0b30\u0b4d',
'r.h':'\u0b30\u0b4d\u200c',
'rA':'\u0b30\u0b3e',
'rI':'\u0b30\u0b40',
'rR':'\u0b30\u0b43',
'rRRi':'\u0b30\u0b43',
'rR^i':'\u0b30\u0b43',
'rU':'\u0b30\u0b42',
'ra':'\u0b30',
'raa':'\u0b30\u0b3e',
'rai':'\u0b30\u0b48',
'rau':'\u0b30\u0b4c',
're':'\u0b30\u0b47',
'ri':'\u0b30\u0b3f',
'rii':'\u0b30\u0b40',
'ro':'\u0b30\u0b4b',
'ru':'\u0b30\u0b41',
'ruu':'\u0b30\u0b42',


'L':'\u0b33\u0b4d',
'L.h':'\u0b33\u0b4d\u200c',
'LA':'\u0b33\u0b3e',
'LI':'\u0b33\u0b40',
'LR':'\u0b33\u0b43',
'LRRi':'\u0b33\u0b43',
'LR^i':'\u0b33\u0b43',
'LU':'\u0b33\u0b42',
'La':'\u0b33',
'Laa':'\u0b33\u0b3e',
'Lai':'\u0b33\u0b48',
'Lau':'\u0b33\u0b4c',
'Le':'\u0b33\u0b47',
'Li':'\u0b33\u0b3f',
'Lii':'\u0b33\u0b40',
'Lo':'\u0b33\u0b4b',
'Lu':'\u0b33\u0b41',
'Luu':'\u0b33\u0b42',


'h':'\u0b39\u0b4d',
'h.h':'\u0b39\u0b4d\u200c',
'hA':'\u0b39\u0b3e',
'hI':'\u0b39\u0b40',
'hR':'\u0b39\u0b43',
'hRRi':'\u0b39\u0b43',
'hR^i':'\u0b39\u0b43',
'hU':'\u0b39\u0b42',
'ha':'\u0b39',
'haa':'\u0b39\u0b3e',
'hai':'\u0b39\u0b48',
'hau':'\u0b39\u0b4c',
'he':'\u0b39\u0b47',
'hi':'\u0b39\u0b3f',
'hii':'\u0b39\u0b40',
'ho':'\u0b39\u0b4b',
'hu':'\u0b39\u0b41',
'huu':'\u0b39\u0b42',


'l':'\u0b32\u0b4d',
'l.h':'\u0b32\u0b4d\u200c',
'lA':'\u0b32\u0b3e',
'lI':'\u0b32\u0b40',
'lR':'\u0b32\u0b43',
'lRRi':'\u0b32\u0b43',
'lR^i':'\u0b32\u0b43',
'lU':'\u0b32\u0b42',
'la':'\u0b32',
'laa':'\u0b32\u0b3e',
'lai':'\u0b32\u0b48',
'lau':'\u0b32\u0b4c',
'le':'\u0b32\u0b47',
'li':'\u0b32\u0b3f',
'lii':'\u0b32\u0b40',
'lo':'\u0b32\u0b4b',
'lu':'\u0b32\u0b41',
'luu':'\u0b32\u0b42',
'ld':'\u0b33\u0b4d',
'ld.h':'\u0b33\u0b4d\u200c',
'ldA':'\u0b33\u0b3e',
'ldI':'\u0b33\u0b40',
'ldR':'\u0b33\u0b43',
'ldRRi':'\u0b33\u0b43',
'ldR^i':'\u0b33\u0b43',
'ldU':'\u0b33\u0b42',
'lda':'\u0b33',
'ldaa':'\u0b33\u0b3e',
'ldai':'\u0b33\u0b48',
'ldau':'\u0b33\u0b4c',
'lde':'\u0b33\u0b47',
'ldi':'\u0b33\u0b3f',
'ldii':'\u0b33\u0b40',
'ldo':'\u0b33\u0b4b',
'ldu':'\u0b33\u0b41',
'lduu':'\u0b33\u0b42',


's':'\u0b38\u0b4d',
's.h':'\u0b38\u0b4d\u200c',
'sA':'\u0b38\u0b3e',
'sI':'\u0b38\u0b40',
'sR':'\u0b38\u0b43',
'sRRi':'\u0b38\u0b43',
'sR^i':'\u0b38\u0b43',
'sU':'\u0b38\u0b42',
'sa':'\u0b38',
'saa':'\u0b38\u0b3e',
'sai':'\u0b38\u0b48',
'sau':'\u0b38\u0b4c',
'se':'\u0b38\u0b47',
'si':'\u0b38\u0b3f',
'sii':'\u0b38\u0b40',
'so':'\u0b38\u0b4b',
'su':'\u0b38\u0b41',
'suu':'\u0b38\u0b42',

'S':'',
'sh':'\u0b36\u0b4d',
'sh.h':'\u0b36\u0b4d\u200c',
'shA':'\u0b36\u0b3e',
'shI':'\u0b36\u0b40',
'shR':'\u0b36\u0b43',
'shRRi':'\u0b36\u0b43',
'shR^i':'\u0b36\u0b43',
'shU':'\u0b36\u0b42',
'sha':'\u0b36',
'shaa':'\u0b36\u0b3e',
'shai':'\u0b36\u0b48',
'shau':'\u0b36\u0b4c',
'she':'\u0b36\u0b47',
'shi':'\u0b36\u0b3f',
'shii':'\u0b36\u0b40',
'sho':'\u0b36\u0b4b',
'shu':'\u0b36\u0b41',
'shuu':'\u0b36\u0b42',

'shh':'\u0b37\u0b4d',
'shh.h':'\u0b37\u0b4d\u200c',
'shhA':'\u0b37\u0b3e',
'shhI':'\u0b37\u0b40',
'shhR':'\u0b37\u0b43',
'shhRRi':'\u0b37\u0b43',
'shhR^i':'\u0b37\u0b43',
'shhU':'\u0b37\u0b42',
'shha':'\u0b37',
'shhaa':'\u0b37\u0b3e',
'shhai':'\u0b37\u0b48',
'shhau':'\u0b37\u0b4c',
'shhe':'\u0b37\u0b47',
'shhi':'\u0b37\u0b3f',
'shhii':'\u0b37\u0b40',
'shho':'\u0b37\u0b4b',
'shhu':'\u0b37\u0b41',
'shhuu':'\u0b37\u0b42',

'Sh':'\u0b37\u0b4d',
'Sh.h':'\u0b37\u0b4d\u200c',
'ShA':'\u0b37\u0b3e',
'ShI':'\u0b37\u0b40',
'ShR':'\u0b37\u0b43',
'ShRRi':'\u0b37\u0b43',
'ShR^i':'\u0b37\u0b43',
'ShU':'\u0b37\u0b42',
'Sha':'\u0b37',
'Shaa':'\u0b37\u0b3e',
'Shai':'\u0b37\u0b48',
'Shau':'\u0b37\u0b4c',
'She':'\u0b37\u0b47',
'Shi':'\u0b37\u0b3f',
'Shii':'\u0b37\u0b40',
'Sho':'\u0b37\u0b4b',
'Shu':'\u0b37\u0b41',
'Shuu':'\u0b37\u0b42',

//************************************************************//
//  rest ungrouped //
//*************************************************************//

'GY':'\u0b1c\u0b4d\u0b1e\u0b4d',
'GY.h':'\u0b1c\u0b4d\u0b1e\u0b4d\u200c',
'GYA':'\u0b1c\u0b4d\u0b1e\u0b3e',
'GYI':'\u0b1c\u0b4d\u0b1e\u0b40',
'GYR':'\u0b1c\u0b4d\u0b1e\u0b43',
'GYRRi':'\u0b1c\u0b4d\u0b1e\u0b43',
'GYR^i':'\u0b1c\u0b4d\u0b1e\u0b43',
'GYU':'\u0b1c\u0b4d\u0b1e\u0b42',
'GYa':'\u0b1c\u0b4d\u0b1e',
'GYaa':'\u0b1c\u0b4d\u0b1e\u0b3e',
'GYai':'\u0b1c\u0b4d\u0b1e\u0b48',
'GYau':'\u0b1c\u0b4d\u0b1e\u0b4c',
'GYe':'\u0b1c\u0b4d\u0b1e\u0b47',
'GYi':'\u0b1c\u0b4d\u0b1e\u0b3f',
'GYii':'\u0b1c\u0b4d\u0b1e\u0b40',
'GYo':'\u0b1c\u0b4d\u0b1e\u0b4b',
'GYu':'\u0b1c\u0b4d\u0b1e\u0b41',
'GYuu':'\u0b1c\u0b4d\u0b1e\u0b42',

//Gaga added J for code to work
'J':'\u0b1e\u0b4d',
'JN':'\u0b1e\u0b4d',
'JN.h':'\u0b1e\u0b4d\u200c',
'JNA':'\u0b1e\u0b3e',
'JNI':'\u0b1e\u0b40',
'JNR':'\u0b1e\u0b43',
'JNRRi':'\u0b1e\u0b43',
'JNR^i':'\u0b1e\u0b43',
'JNU':'\u0b1e\u0b42',
'JNa':'\u0b1e',
'JNaa':'\u0b1e\u0b3e',
'JNai':'\u0b1e\u0b48',
'JNau':'\u0b1e\u0b4c',
'JNe':'\u0b1e\u0b47',
'JNi':'\u0b1e\u0b3f',
'JNii':'\u0b1e\u0b40',
'JNo':'\u0b1e\u0b4b',
'JNu':'\u0b1e\u0b41',
'JNuu':'\u0b1e\u0b42',





//Gaga added w
'w':'\u0b71\u0b4d',
'w.h':'\u0b71\u0b4d\u200c',
'wA':'\u0b71\u0b3e',
'wI':'\u0b71\u0b40',
'wR':'\u0b71\u0b43',
'wRRi':'\u0b71\u0b43',
'wR^i':'\u0b71\u0b43',
'wU':'\u0b71\u0b42',
'wa':'\u0b71',
'waa':'\u0b71\u0b3e',
'wai':'\u0b71\u0b48',
'wau':'\u0b71\u0b4c',
'we':'\u0b71\u0b47',
'wi':'\u0b71\u0b3f',
'wii':'\u0b71\u0b40',
'wo':'\u0b71\u0b4b',
'wu':'\u0b71\u0b41',
'wuu':'\u0b71\u0b42',

'v':'\u0b35\u0b4d',
'v.h':'\u0b35\u0b4d\u200c',
'vA':'\u0b35\u0b3e',
'vI':'\u0b35\u0b40',
'vR':'\u0b35\u0b43',
'vRRi':'\u0b35\u0b43',
'vR^i':'\u0b35\u0b43',
'vU':'\u0b35\u0b42',
'va':'\u0b35',
'vaa':'\u0b35\u0b3e',
'vai':'\u0b35\u0b48',
'vau':'\u0b35\u0b4c',
've':'\u0b35\u0b47',
'vi':'\u0b35\u0b3f',
'vii':'\u0b35\u0b40',
'vo':'\u0b35\u0b4b',
'vu':'\u0b35\u0b41',
'vuu':'\u0b35\u0b42',

//End of Gaga added w

'x':'\u0b15\u0b4d\u0b37\u0b4d',
'x.h':'\u0b15\u0b4d\u0b37\u0b4d\u200c',
'xA':'\u0b15\u0b4d\u0b37\u0b3e',
'xI':'\u0b15\u0b4d\u0b37\u0b40',
'xR':'\u0b15\u0b4d\u0b37\u0b43',
'xRRi':'\u0b15\u0b4d\u0b37\u0b43',
'xR^i':'\u0b15\u0b4d\u0b37\u0b43',
'xU':'\u0b15\u0b4d\u0b37\u0b42',
'xa':'\u0b15\u0b4d\u0b37',
'xaa':'\u0b15\u0b4d\u0b37\u0b3e',
'xai':'\u0b15\u0b4d\u0b37\u0b48',
'xau':'\u0b15\u0b4d\u0b37\u0b4c',
'xe':'\u0b15\u0b4d\u0b37\u0b47',
'xi':'\u0b15\u0b4d\u0b37\u0b3f',
'xii':'\u0b15\u0b4d\u0b37\u0b40',
'xo':'\u0b15\u0b4d\u0b37\u0b4b',
'xu':'\u0b15\u0b4d\u0b37\u0b41',
'xuu':'\u0b15\u0b4d\u0b37\u0b42',

//VEDIC ACCENT MARKS NOT UNICODED YET
'{\\':'\ue001',
'{\M':'\ue001',
'{\M+':'\ue001',
'{\M+}':'\ue001',
'{\m':'\ue001',
'{\m+':'\ue001',
'{\m+}':'\ue001',
};
