
$(window).load(function () {
    $('#divBody').delegate('textarea', 'change keyup click', function () {
        transliterate();
    });
});


function loadFooter() {
    var str = 'All material copyright Â© Odia.org 2008 - ' + new Date().getFullYear();
    var ftr = document.getElementById('footerSpan');
    ftr.innerHTML = str;

    $("#DivJuktaKeypad").hide();
    $("#showJukta").show();
    $("#hideJukta").hide();
}


$(document).ready(function () {
    $("#TextAreaITran").focus();
});



function transliterate() {

    var src = document.getElementById('TextAreaITran');
    var d = document.getElementById('TextAreaOdia');
    d.value = convertToOdia(src.value);
    
    var option1src = document.getElementById('option1Answer');
    var option1d = document.getElementById('option1AnswerOdia');
    option1d.value = convertToOdia(option1src.value);
    
    var option2src = document.getElementById('option2Answer');
    var option2d = document.getElementById('option2AnswerOdia');
    option2d.value = convertToOdia(option2src.value);
    
    var option3src = document.getElementById('option3Answer');
    var option3d = document.getElementById('option3AnswerOdia');
    option3d.value = convertToOdia(option3src.value);
    
    var option4src = document.getElementById('option4Answer');
    var option4d = document.getElementById('option4AnswerOdia');
    option4d.value = convertToOdia(option4src.value);
    

}

function convertToOdia(inputStr) {

    var odiaKeyMap = Oriya_kmap;
    var currentChar = '';
    var odiaStr = '';
    var oVal;
    var englishEscape = false;

    if (inputStr.length > 0) {
        for (var i = 0; i < inputStr.length; i++) {

            // Check escape sequence '##'
            if ((inputStr.charAt(i)) == '#') {

                if (i + 1 <= inputStr.length) {
                    var seq = inputStr.substring(i, i + 2);
                    if (seq == "##")
                        englishEscape = true;
                }
            }
            else
                englishEscape = false;


            if (englishEscape == false) {

                // Generic for loop here to go from 6 to 1 to replace the 6 blocks below
                var greedyStr = '';
                for (var j = 6; j > 0; j--) {
                    if (i + j <= inputStr.length) {
                        greedyStr = inputStr.substring(i, i + j);
                        oVal = odiaKeyMap[greedyStr];
                        if (oVal != undefined) { // if matched
                            // display output
                            odiaStr += oVal;
                            // increase counter
                            i += (j - 1);
                            // and move to next iteration
                            break;
                        }
                        else {
                            if (j == 1)
                                odiaStr += greedyStr;
                        }
                    }
                }
            }
            else { // render english script

                // Get the next occurence of ##
                var remainingString = inputStr.substring(i + 2, inputStr.length);

                var location = remainingString.indexOf('##');
                if (location == -1) // not found
                {
                    //dump  the entire remaining string as is 
                    odiaStr += remainingString;
                    // end the for
                    break;
                }
                else {
                    // Dump all the characters as is
                    var englishStr = remainingString.substring(0, location);
                    odiaStr += englishStr;
                    // Move the FOR counter to beyond ##                    
                    i += location + 3;
                }

                englishEscape = false;
            }
        }
    }

    return odiaStr;
}

