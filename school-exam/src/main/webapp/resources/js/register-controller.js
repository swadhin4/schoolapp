
var loginApp = angular.module("loginApp", []);

loginApp.controller('registerController', function($rootScope, $scope, $http,
		$filter, $q) {

	$scope.user={};
	$scope.loginuser={};
	$scope.boardName={
		 selected:'',
		 values:[]
	};
	
	$scope.standard={
			selected:'',
			values:[]
	};
	angular.element(document).ready(function () {
		$scope.getAllBoards();
    });
	
	$scope.getAllBoards=function(){
		
		var response=$http.get('/board/all');
		response.success(function(data,status){
			console.log(JSON.stringify(data));
			$scope.boardName.values=data;
		});
		
		//For Testing sample data
		// var boards=[{"code":"BSE","name":"Odisha Board ( BSE )"}];
		// $scope.boardName.values=boards;
		
		
	};
	
	$scope.getBoardStandard=function(selectedBoard){
		//alert($scope.boardName.selected);
		/*var response=$http.get('/app/board/standard/'+selectedBoard.id);
		response.success(function(data,status){
			console.log(JSON.stringify(data));
			$scope.standard.values=data;
		});*/
		var selectedClass=[{"code":10,"name":"Class 10"}];
		$scope.standard.values=selectedClass;

	};
	
	$scope.doSubmit=function(){
			console.log(JSON.stringify($scope.user));
			if($scope.registerForm.$valid){
			var masterDataStudent={
				firstName:$scope.user.firstName,
				lastName:$scope.user.lastName,
				board:$scope.boardName.selected,
				standard:$scope.standard.selected.name,
				instituteName:$scope.user.instituteName,
				email:$scope.user.email,
				mobile:$scope.user.mobile,
				couponCode:$scope.user.couponCode
			}
			console.log(JSON.stringify(masterDataStudent));
			var res=$http.post('/register/user',masterDataStudent);
			res.success(function(data,status, header, config){
				console.log(JSON.stringify(data));
				if(data.statusCode=="200"){
					$('#credentials').empty();
					$('#exampleModalLabel').text("Success")
					$('#headerdiv').addClass('successGreen')
					$('#headerdiv').removeClass('errorRed');;
					$('#credentials').text("Your Username is: " + data.object.username + " and Password is "+ data.object.password);
					$('#confirmBtn').click();
				}
				else if(data.statusCode=="500"){
					$('#credentials').empty();
					$('#exampleModalLabel').text("Invalid")
					$('#headerdiv').addClass('errorRed')
					$('#headerdiv').removeClass('successGreen');;
					for( var key in student){
						if(masterDataStudent[key]==null && key !='email' && key !='couponCode'){
							$('#credentials').append(  key + " cannot be blank <br>");
						}
					}
					
				$('#confirmBtn').click();
				}
				
				else{
					if(data.statusCode=="201"){
						$('#credentials').empty();
						$('#headerdiv').addClass('errorRed');
						$('#headerdiv').removeClass('successGreen');;
						$('#exampleModalLabel').text("Error")
						$('#credentials').text("Error : " + data.message);
						$('#confirmBtn').click()
					}
				}
				//$('#successDivText').text("Thank you for registration. Your username is: "+data.username+" and password is: "+data.password);
			})
			res.error(function(status){
				
			})
			}
	};
	
	$scope.doLoginSubmit=function(){
		console.log(JSON.stringify($scope.loginuser));
		if($scope.loginForm.$valid){
		var loginUserData={
			username:$scope.loginuser.username,
			password:$scope.loginuser.password,
		};
		console.log(JSON.stringify(loginUserData));
		 $.ajax({
			 url:'j_spring_security_check',
			 type:'post',
			 data:JSON.stringify(loginUserData),
			 contentType:'application/json',
			 async:false,
			 success:function(data){
				if(data.statusCode=="200"){
					window.location.href="home";
				}else if(data.statusCode=="201"){
					$('#invalidLoginalert').text(data.message);
					$('#invalidLoginalert').show();
				}
			 }
		 })
		
	}
 };

});
