var adminApp=angular.module("adminApp", []);

adminApp.controller('adminController',  function($rootScope,$scope,$http,$filter,$q) {
	
	$scope.getOriyaText=function(){
		console.log($('#TextAreaOdia').val());
	}
	
	$scope.allBoards;
	$scope.selected={
			board:""
	}
	 $scope.boardselections = "";
	$http.get("/app/board/list").
	success(function(data){
		$scope.allBoards=data;
		console.log($scope.allBoards);
	});
		
	$scope.selectedSubject={
			name:""
	}
	$scope.allSubjects;
	$scope.allTopics;
	$scope.getSubjects=function(){
		console.log($scope.selected.board);
		var board=JSON.parse($scope.selected.board);
		$http.get('/app/board/subject/'+board.id).
		success(function(data) {
			console.log(data);
			$scope.allSubjects=data;
			//$scope.allTopics=data.topics;
		});
	}
	
	$scope.getTopics=function(){
		$scope.allTopics;
		console.log($scope.selectedSubject.name);
		var subject=JSON.parse($scope.selectedSubject.name);
			$scope.allTopics=subject.topics;
	}
	$scope.selectedTopic={
			name:""
	}
	$scope.isTopic=false;
	$scope.allQuestions;
	$scope.totalQuestion;
	$scope.countTotalQuestions=function(){
		$scope.isTopic=true
		$scope.allQuestions;
		$scope.totalQuestion;
		console.log($scope.selectedTopic.name);
		var topic=JSON.parse($scope.selectedTopic.name);
			$scope.allQuestions=topic.questionList;
			$scope.totalQuestion=$scope.allQuestions.length
	}
	
	$scope.odia;
	$scope.createQuestion=function(){
	
		var ans=$('#TextAreaOdia').val();
		alert("ans")
		var html_txt = "";
  for (i=0; i<ans.length; i++) {
    var unicode_character = ans.charCodeAt(i);
    switch (unicode_character) {
    case 32:
      html_txt += " ";
      break;
    case 10:
    case 13:
      html_txt += "<br/>\n";
      break;
    default:
      html_txt += "&#" + unicode_character + ";";
    }
  }
 $('#html_text').val(html_txt);

  var questionData={
				//board:$scope.selected.board,
			//	subject:$scope.selectedSubject.name,
				topic:$scope.selectedTopic.name.name,
				question:$('#html_text').val(),
				option1:$('#option1AnswerOdia').val(),
				option2:$('#option2AnswerOdia').val(),
				option3:$('#option3AnswerOdia').val(),
				option4:$('#option4AnswerOdia').val(),
				answer:$('#correctAnswer').val(),
				
		}
		console.log(questionData);
		
		$.ajax({
			url:"/app/question/save",
			type:"post",
			headers: { 
		        'Accept': 'application/json',
		        'Content-Type': 'application/json' 
		    },
			data:JSON.stringify(questionData),
			success:function(data){
				console.log("Data : " +data);
			}
		})
		/*$http.post("/app/question/save",JSON.stringify(questionData)).*/
		
	}
});

