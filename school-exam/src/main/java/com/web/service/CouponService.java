package com.web.service;

import com.jpa.entities.Coupon;

public interface CouponService {

	public Coupon save(Coupon coupon);

	public Coupon findByCouponCode(String code);
}
