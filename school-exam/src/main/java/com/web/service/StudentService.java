package com.web.service;

import java.util.List;

import com.jpa.entities.Student;
import com.web.form.StudentDTO;

public interface StudentService {

	public StudentDTO save(StudentDTO studentDTO) throws Exception;

	public List<StudentDTO> findAll();

	public Student findStudent(String username);

}
