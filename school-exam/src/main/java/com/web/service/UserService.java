/*
 * Copyright (C) 2013 , Inc. All rights reserved 
 */
package com.web.service;

import java.util.List;

import com.jpa.entities.User;
import com.web.views.UserVO;

public interface UserService extends BaseService<User> {
	List<User> findALL();

	User save(User user);

	@Override
	User retrieve(Long userId);

	User update(User user);

	User findByUserName(String userName);

	User authenticate(UserVO userVO) throws Exception;

	User findByEmail(String email);

	User getCurrentLoggedinUser();
}
