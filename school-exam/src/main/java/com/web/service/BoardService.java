package com.web.service;

import java.util.List;

import com.jpa.entities.Board;

public interface BoardService {

	public Board findByName(String board);

	public List<Board> findAll();

	public Board findById(Long boardid);
}
