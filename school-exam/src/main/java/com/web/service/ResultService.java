package com.web.service;

import java.util.List;

import com.jpa.entities.Subject;
import com.jpa.entities.TestResult;
import com.jpa.entities.User;

public interface ResultService {

	public TestResult findResult(User user, Subject subject) throws Exception;

	public Long getAttemptCount(User user, Subject subject) throws Exception;

	public Long getMaxScore(User user, Subject subject) throws Exception;

	public TestResult saveResult(TestResult testResult);

	public List<TestResult> findResultList(User user);

}
