package com.web.service.impl;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jpa.entities.Role;
import com.jpa.entities.Student;
import com.jpa.entities.User;
import com.jpa.entities.UserRole;
import com.jpa.repositories.RoleDAO;
import com.jpa.repositories.StudentDAO;
import com.jpa.repositories.UserDAO;
import com.web.form.StudentDTO;
import com.web.service.StudentService;
import com.web.util.RandomUtils;

@Service("studentService")
public class StudentServiceImpl implements StudentService {

	@Autowired
	private StudentDAO studentDAO;

	@Autowired
	private UserDAO userDAO;

	@Autowired
	private RoleDAO roleDAO;

	@Override
	public StudentDTO save(StudentDTO studentDTO) throws Exception {
		Student student = new Student();
		BeanUtils.copyProperties(studentDTO, student);
		User user = new User();
		user.setUserName(studentDTO.getFirstName()
				+ RandomUtils.randomAlphanumeric(4));
		user.setPassword(RandomUtils.randomAlphanumeric(8));
		user.setEnabled(true);
		Role role = roleDAO.findOne(2L);
		UserRole userRole = new UserRole();
		userRole.setRole(role);
		user.setUserRole(userRole);
		userRole.setUser(user);

		user.setStudent(student);
		student.setUser(user);
		student = studentDAO.save(student);
		BeanUtils.copyProperties(student, studentDTO);
		studentDTO.setUsername(student.getUser().getUserName());
		studentDTO.setPassword(student.getUser().getPassword());
		return studentDTO;
	}

	@Override
	public List<StudentDTO> findAll() {
		return null;
	}

	@Override
	public Student findStudent(String username) {
		User user = userDAO.findByUserName(username);
		return user.getStudent();
	}
}
