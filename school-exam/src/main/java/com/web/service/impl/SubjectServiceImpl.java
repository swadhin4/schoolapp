package com.web.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jpa.entities.Board;
import com.jpa.entities.Subject;
import com.jpa.repositories.SubjectDAO;
import com.web.service.SubjectService;

@Service("subjectService")
public class SubjectServiceImpl implements SubjectService {

	@Autowired
	private SubjectDAO subjectDAO;

	@Override
	public List<Map<Integer, String>> findSubjectByBoard(Integer board) {
		return subjectDAO.findBoardSubjects(board.longValue());
	}

	@Override
	public Subject findSubject(Long subjectid) {
		// TODO Auto-generated method stub
		return subjectDAO.findOne(subjectid);
	}

	@Override
	public List<Subject> findByBoard(Board board) {
		// TODO Auto-generated method stub
		return subjectDAO.findByBoard(board);
	}

}
