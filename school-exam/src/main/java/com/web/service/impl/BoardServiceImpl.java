package com.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jpa.entities.Board;
import com.jpa.repositories.BoardDAO;
import com.web.service.BoardService;

@Service("boardService")
public class BoardServiceImpl implements BoardService {

	@Autowired
	private BoardDAO boardDAO;

	@Override
	public Board findByName(String board) {
		return boardDAO.findByName(board);
	}

	@Override
	public List<Board> findAll() {
		return boardDAO.findAll();
	}

	@Override
	public Board findById(Long boardid) {
		// TODO Auto-generated method stub
		return boardDAO.findOne(boardid);
	}
}
