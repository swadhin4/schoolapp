package com.web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jpa.entities.Coupon;
import com.jpa.repositories.CouponDAO;
import com.web.service.CouponService;

@Service(value = "couponService")
public class CouponServiceImpl implements CouponService {

	@Autowired
	private CouponDAO couponDAO;

	@Override
	public Coupon save(Coupon coupon) {
		return null;
	}

	@Override
	public Coupon findByCouponCode(String code) {
		return couponDAO.findByCouponCode(code);
	}

}
