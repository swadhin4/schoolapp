package com.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jpa.entities.Subject;
import com.jpa.entities.TestResult;
import com.jpa.entities.User;
import com.jpa.repositories.TestResultDAO;
import com.web.service.ResultService;

@Service(value = "resultService")
public class ResultServiceImpl implements ResultService {

	@Autowired
	private TestResultDAO testResultDAO;

	@Override
	public TestResult findResult(User user, Subject subject) throws Exception {
		List<TestResult> resultList = testResultDAO.findByUser(user);
		System.out.println("ResultList : " + resultList);
		return null;
	}

	@Override
	public Long getAttemptCount(User user, Subject subject) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long getMaxScore(User user, Subject subject) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TestResult saveResult(TestResult testResult) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TestResult> findResultList(User user) {
		// TODO Auto-generated method stub
		return testResultDAO.findByUser(user);
	}

}
