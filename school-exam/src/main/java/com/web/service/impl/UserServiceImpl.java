package com.web.service.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jpa.entities.User;
import com.jpa.repositories.UserDAO;
import com.web.service.UserService;
import com.web.util.ServiceUtil;
import com.web.views.UserVO;

@Service("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDAO userDAO;

	@Override
	public List<User> findALL() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User save(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User update(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User findByUserName(final String userName) {
		return userDAO.findByUserName(userName);
	}

	@Override
	public User findByEmail(String email) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User retrieve(Long userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User authenticate(UserVO userVO) throws Exception {
		boolean userValid = false;
		User user = null;
		try {
			user = userDAO.findByUserName(userVO.getUserName());
			if (user != null) {
				userValid = isValidUser(user, userVO);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (userValid) {
			return user;
		}
		return user;

	}

	private boolean isValidUser(User user, UserVO userVO) {
		if (user.getUserName().equals(userVO.getUserName())
				&& user.getPassword().equals(userVO.getPassword())) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public User saveOrUpdate(User baseEntity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(User baseEntity) {
		// TODO Auto-generated method stub

	}

	@Transactional(readOnly = true)
	@Override
	public User getCurrentLoggedinUser() {
		String username = null;
		try {
			username = ServiceUtil.getCurrentLoggedinUserName();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (StringUtils.isBlank(username)) {
			return null;
		}
		return findByUserName(username);
	}

}
