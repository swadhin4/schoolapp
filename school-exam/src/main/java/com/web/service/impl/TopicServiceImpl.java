package com.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jpa.entities.Subject;
import com.jpa.entities.Topic;
import com.jpa.repositories.SubjectDAO;
import com.jpa.repositories.TopicDAO;
import com.web.service.TopicService;

@Service("topicService")
public class TopicServiceImpl implements TopicService {

	@Autowired
	private TopicDAO topicDAO;

	@Autowired
	private SubjectDAO subjectDAO;

	@Override
	@Transactional
	public List<Topic> findTopics(Long subjectid) {
		Subject subject = subjectDAO.findOne(subjectid);
		return subject.getTopics();
	}

	@Override
	@Transactional
	public Topic findById(Long topicid) {
		return topicDAO.findOne(topicid);
	}

}
