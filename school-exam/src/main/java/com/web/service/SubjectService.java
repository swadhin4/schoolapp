package com.web.service;

import java.util.List;
import java.util.Map;

import com.jpa.entities.Board;
import com.jpa.entities.Subject;

public interface SubjectService {

	public List<Map<Integer, String>> findSubjectByBoard(Integer board);

	public Subject findSubject(Long subjectid);

	public List<Subject> findByBoard(Board board);

}
