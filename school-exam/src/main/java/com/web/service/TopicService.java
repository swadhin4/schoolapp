package com.web.service;

import java.util.List;

import com.jpa.entities.Topic;

public interface TopicService {

	public List<Topic> findTopics(Long subjectid);

	public Topic findById(Long topicid);
}
