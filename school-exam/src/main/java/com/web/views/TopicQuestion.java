package com.web.views;

import java.util.ArrayList;
import java.util.List;

import com.jpa.entities.Question;

public class TopicQuestion {

	private Long id;

	private String name;

	private List<Question> questionList = new ArrayList<Question>(0);

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Question> getQuestionList() {
		return questionList;
	}

	public void setQuestionList(List<Question> questionList) {
		this.questionList = questionList;
	}

	@Override
	public String toString() {
		return "TopicQuestion [id=" + id + ", name=" + name + ", questionList="
				+ questionList + "]";
	}

}
