package com.web.form;

import java.math.BigDecimal;

public class StudentDTO {

	private Long id;
	private String firstName;
	private String lastName;
	private String board;
	private String instituteName;
	private String standard;
	private String email;
	private String mobile;
	private String copounCode;
	private BigDecimal balance;
	private String username;
	private String password;
	private String createdDate;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	public String getBoard() {
		return board;
	}

	public void setBoard(final String board) {
		this.board = board;
	}

	public String getStandard() {
		return standard;
	}

	public void setStandard(final String standard) {
		this.standard = standard;
	}

	public String getInstituteName() {
		return instituteName;
	}

	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCopounCode() {
		return copounCode;
	}

	public void setCopounCode(String copounCode) {
		this.copounCode = copounCode;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	@Override
	public String toString() {
		return "StudentDTO [firstName=" + firstName + ", lastName=" + lastName
				+ ", board=" + board + ", standard=" + standard + ", email="
				+ email + ", mobile=" + mobile + "]";
	}

}
