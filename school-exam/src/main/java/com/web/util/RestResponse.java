package com.web.util;

public class RestResponse {

	private int statusCode;

	private String message;

	private Object object;

	public RestResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RestResponse(int i, String message) {
		super();
		this.statusCode = i;
		this.message = message;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

}
