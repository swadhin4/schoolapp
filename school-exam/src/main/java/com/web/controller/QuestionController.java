package com.web.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jpa.entities.Question;
import com.jpa.entities.Subject;
import com.jpa.entities.Topic;
import com.jpa.entities.User;
import com.web.service.TopicService;
import com.web.service.UserService;
import com.web.util.RestResponse;

@Controller
@RequestMapping(value = "/question")
public class QuestionController {

	private static final Logger logger = LoggerFactory
			.getLogger(QuestionController.class);

	@Autowired
	private TopicService topicService;

	@Autowired
	private UserService userService;

	/*
	 * @RequestMapping(value = "/start", method = RequestMethod.GET)
	 * 
	 * @ResponseBody public RestResponse home(Locale locale, Model model,
	 * HttpServletRequest request) { User user =
	 * userService.getCurrentLoggedinUser(); RestResponse response = new
	 * RestResponse();
	 * 
	 * if (user != null) { Map<Integer, String> answerList = new
	 * HashMap<Integer, String>(0);
	 * request.getSession().setAttribute("answerList", answerList);
	 * response.setStatusCode(200); } else { response.setStatusCode(201); }
	 * 
	 * return response; }
	 */

	@RequestMapping(value = "/start", method = RequestMethod.GET)
	public String home(Locale locale, Model model, HttpServletRequest request) {
		User user = userService.getCurrentLoggedinUser();
		if (user != null) {
			Subject selectedSubject = (Subject) request.getSession()
					.getAttribute("subject");

			model.addAttribute("subject", selectedSubject);
			Map<Integer, String> answerList = new HashMap<Integer, String>(0);
			request.getSession().setAttribute("answerList", answerList);
			return "question.window";
		} else {
			return "redirect:/";
		}

	}

	@RequestMapping(value = "/topic/{topics}/{maxQuestion}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public RestResponse questionList(Locale locale, Model model,
			HttpServletRequest request,
			@PathVariable(value = "topics") String topics,
			@PathVariable(value = "maxQuestion") Long maxQuestion) {
		logger.info("Inside Topic Selection");
		RestResponse response = new RestResponse();
		User user = userService.getCurrentLoggedinUser();
		if (user != null) {
			List<Question> selectedQuestionList = new ArrayList<Question>(0);
			logger.info("topicQuestion : " + topics);
			String[] topicIds = topics.split(",");
			int totalQuestions = maxQuestion.intValue();
			logger.info("Total Question: " + totalQuestions);
			int eachTopicCount = totalQuestions / topicIds.length;
			logger.info("eachTopicCount: " + eachTopicCount);
			for (String topic : topicIds) {
				Topic savedTopic = topicService.findById(Long.parseLong(topic));
				Collections.shuffle(savedTopic.getQuestionList());
				for (int i = 0; i < eachTopicCount; i++) {
					logger.info("Got record : "
							+ savedTopic.getQuestionList().get(i).getId());

					selectedQuestionList.add(savedTopic.getQuestionList()
							.get(i));

				}
				logger.info("Total Question :  " + selectedQuestionList.size());
			}
			model.addAttribute("questionList", selectedQuestionList);
			response.setObject(selectedQuestionList);
			response.setStatusCode(200);
		} else {
			response.setStatusCode(000);
			response.setMessage("User not logged in");
		}
		return response;
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public RestResponse questions(Locale locale, Model model,
			HttpServletRequest request, @RequestBody String jsonData) {
		RestResponse response = new RestResponse();
		if (request.getSession().getAttribute("username") != null) {
			JSONParser parser = new JSONParser();
			JSONObject json;
			try {
				json = (JSONObject) parser.parse(jsonData);
				System.out.println(json.get("question").toString());

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return response;
	}

	public static String getUnicodeValue(String line) {

		String hexCodeWithLeadingZeros = "";
		try {
			for (int index = 0; index < line.length(); index++) {
				String hexCode = Integer.toHexString(line.codePointAt(index))
						.toUpperCase();
				String hexCodeWithAllLeadingZeros = "0000" + hexCode;
				String temp = hexCodeWithAllLeadingZeros
						.substring(hexCodeWithAllLeadingZeros.length() - 4);
				hexCodeWithLeadingZeros += ("\\u" + temp);
			}
			return hexCodeWithLeadingZeros;
		} catch (NullPointerException nlpException) {
			return hexCodeWithLeadingZeros;
		}
	}

}
