package com.web.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.jpa.entities.Question;
import com.jpa.entities.Subject;
import com.jpa.entities.Topic;
import com.jpa.entities.User;
import com.web.service.SubjectService;
import com.web.service.TopicService;
import com.web.service.UserService;

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping(value = "/test")
public class TestController {

	private static final Logger logger = LoggerFactory
			.getLogger(TestController.class);

	@Autowired
	private SubjectService subjectService;

	@Autowired
	private UserService userService;

	@Autowired
	private TopicService topicService;

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/begin", method = RequestMethod.POST)
	public String home(Locale locale, ModelMap model,
			@RequestParam(value = "subjectcode") String subjectcode,
			@RequestParam(value = "queval") String queval,
			@RequestParam(value = "topics") String topics,
			@RequestParam(value = "maxQuestion") Long maxQuestion,
			HttpServletRequest request) {
		User user = userService.getCurrentLoggedinUser();
		if (user != null) {
			model.addAttribute("sessioncode", subjectcode);
			Subject subject = subjectService
					.findSubject(Long.parseLong(queval));
			List<Question> selectedQuestionList = new ArrayList<Question>(0);
			logger.info("topicQuestion : " + topics);
			String[] topicIds = topics.split(",");
			int totalQuestions = maxQuestion.intValue();
			logger.info("Total Question: " + totalQuestions);
			int eachTopicCount = totalQuestions / topicIds.length;
			logger.info("eachTopicCount: " + eachTopicCount);
			for (String topic : topicIds) {
				Topic savedTopic = topicService.findById(Long.parseLong(topic));
				Collections.shuffle(savedTopic.getQuestionList());
				for (int i = 0; i < eachTopicCount; i++) {
					logger.info("Got record : "
							+ savedTopic.getQuestionList().get(i).getId());

					selectedQuestionList.add(savedTopic.getQuestionList()
							.get(i));

				}
				logger.info("Total Question :  " + selectedQuestionList.size());
			}
			request.getSession().setAttribute("questionList",
					selectedQuestionList);
			request.getSession().setAttribute("subject", subject);
			return "test.entry";
		} else {
			return "redirect:/";
		}
	}

}
