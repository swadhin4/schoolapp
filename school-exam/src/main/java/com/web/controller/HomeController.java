package com.web.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jpa.entities.Question;
import com.jpa.entities.Student;
import com.jpa.entities.User;
import com.web.form.StudentDTO;
import com.web.service.StudentService;
import com.web.service.UserService;
import com.web.service.impl.UtilityService;

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping(value = "/home")
public class HomeController extends BaseController {

	private static final Logger logger = LoggerFactory
			.getLogger(HomeController.class);

	@Autowired
	private StudentService studentService;

	@Autowired
	private UserService userService;

	@Autowired
	private UtilityService utilityService;

	/**
	 * Simply selects the home view to render by returning its name.
	 */

	@RequestMapping(value = "/user", method = RequestMethod.GET)
	public String login(Locale locale, ModelMap model,
			HttpServletRequest request) {
		User user = userService.getCurrentLoggedinUser();
		/*
		 * System.out.println(request.getSession().isNew()); if
		 * (request.getSession().getAttribute("username") != null) {
		 * List<Question> questionList = new ArrayList<Question>(0);
		 * request.getSession().setAttribute("questionList", questionList);
		 * return "home"; } else { return "redirect:/"; }
		 */
		if (user == null) {
			return "redirect:/login";
		} else if (utilityService.isSuperUser()) {
			return "redirect:/admin/home";
		} else {
			List<Question> questionList = new ArrayList<Question>(0);
			request.getSession().setAttribute("questionList", questionList);
			return "home";
		}
	}

	@RequestMapping(value = "/student/details", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public StudentDTO studentDetails(Locale locale, ModelMap model,
			HttpServletRequest request) {
		Student student = null;
		User user = userService.getCurrentLoggedinUser();
		if (user != null) {
			student = studentService.findStudent(user.getUserName());
			StudentDTO studentDTO = new StudentDTO();
			Date date = student.getCreatedAt();
			SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yy");
			String dateText = df2.format(date);
			System.out.println(dateText);
			studentDTO.setCreatedDate(dateText);
			BeanUtils.copyProperties(student, studentDTO);
			return studentDTO;
		} else {
			return null;
		}
	}

}
