package com.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.web.service.UserService;

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping(value = "/application")
public class LogoutController {

	private static final Logger logger = LoggerFactory
			.getLogger(LogoutController.class);

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String home(ModelMap model, HttpServletRequest request,
			HttpServletResponse res, HttpSession session) {
		String user = (String) request.getSession().getAttribute("username");
		if (user != null) {
			session.invalidate();
		}
		return "redirect:/";
	}
}
