package com.web.controller;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.web.form.StudentDTO;
import com.web.service.CouponService;
import com.web.service.StudentService;
import com.web.util.RestResponse;

/**
 */
@Controller
@RequestMapping(value = "/register")
public class RegisterController {

	private static final Logger logger = LoggerFactory
			.getLogger(RegisterController.class);

	@Autowired
	private StudentService studentService;

	@Autowired
	private CouponService couponService;

	@RequestMapping(value = "/user", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody
	RestResponse register(Model model, @RequestBody StudentDTO studentDto) {
		System.out.println(studentDto);
		StudentDTO studentDTO = null;
		RestResponse response = new RestResponse();
		try {

			if (validateFields(studentDto)) {
				response.setStatusCode(500);
				response.setMessage("Please enter valid information");

			} else {
				studentDTO = studentService.save(studentDto);
				response.setStatusCode(200);
				response.setObject(studentDTO);
				/*
				 * Coupon coupon = couponService.findByCouponCode(studentDto
				 * .getCopounCode()); boolean isValidCoupon = false; if
				 * (coupon.getStatus().equals(Status.USED)) { isValidCoupon =
				 * false; } else if (coupon.getStatus().equals(Status.NOT_USED))
				 * { isValidCoupon = true; } else if
				 * (coupon.getStatus().equals(Status.EXPIRED)) { isValidCoupon =
				 * false; } if (isValidCoupon) {
				 */

			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			response.setStatusCode(201);
			response.setMessage("User already exists");
		}
		return response;

	}

	private boolean validateFields(StudentDTO studentDto) {
		boolean inValidFields = false;
		if (StringUtils.isBlank(studentDto.getFirstName())) {
			inValidFields = true;
		} else if (StringUtils.isBlank(studentDto.getLastName())) {
			inValidFields = true;
		} else if (StringUtils.isBlank(studentDto.getInstituteName())) {
			inValidFields = true;
		}

		else if (StringUtils.isBlank(studentDto.getBoard())) {
			inValidFields = true;
		}

		else if (StringUtils.isBlank(studentDto.getStandard())) {
			inValidFields = true;
		}

		else if (StringUtils.isBlank(studentDto.getMobile())) {
			inValidFields = true;
		}
		return inValidFields;
	}

}
