package com.web.controller;

import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.web.service.ResultService;
import com.web.service.StudentService;
import com.web.service.UserService;

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping(value = "/result")
public class ResultController {

	private static final Logger logger = LoggerFactory
			.getLogger(ResultController.class);

	@Autowired
	private StudentService studentService;

	@Autowired
	private UserService userService;

	@Autowired
	private ResultService resultService;

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	private static Integer successCount = 0;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveResult(Locale locale, Model model,
			HttpServletRequest request) {
		System.out.println(request.getParameter("testResult"));
		return null;
	}

	@RequestMapping(value = "/store", method = RequestMethod.GET, produces = "application/json")
	public void storeResult(ModelMap modelMap,
			@RequestParam("questionValues") String questionValues,
			HttpServletRequest request) {
		if (request.getSession().getAttribute("username") != null) {
			Map<Integer, String> answerList = (Map<Integer, String>) request
					.getSession().getAttribute("answerList");
			JSONObject jsonObject = new JSONObject();
			JSONParser parser = new JSONParser();
			try {
				jsonObject = (JSONObject) parser.parse(questionValues);
				System.out.println(jsonObject.get("question"));
				Integer que = Integer.parseInt(jsonObject.get("question")
						.toString());
				String value = jsonObject.get("value").toString();
				String correctAns = jsonObject.get("correctAns").toString();
				if (!answerList.isEmpty() && answerList.containsKey(que)) {
					answerList.put(que, value);
				} else {
					answerList.put(que, value);
				}

				if (correctAns.equalsIgnoreCase(value)) {
					successCount++;
				}
				System.out.println("answerList : " + answerList);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Answers : " + answerList.size() + " Correct : "
					+ successCount + " Wrong : "
					+ (answerList.size() - successCount));

		}
	}
}
