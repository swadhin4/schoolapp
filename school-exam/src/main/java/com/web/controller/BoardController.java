package com.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jpa.entities.Board;
import com.jpa.entities.Subject;
import com.web.service.BoardService;
import com.web.service.StudentService;
import com.web.service.SubjectService;
import com.web.service.TopicService;
import com.web.util.RandomUtils;
import com.web.util.RestResponse;

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping(value = "/board")
public class BoardController {

	private static final Logger logger = LoggerFactory
			.getLogger(BoardController.class);

	@Autowired
	private StudentService studentService;

	@Autowired
	private SubjectService subjectService;

	@Autowired
	private BoardService boardService;

	@Autowired
	private TopicService topicService;

	/**
	 * Simply selects the home view to render by returning its name.
	 */

	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Board> allBoard(Locale locale, Model model,
			HttpServletRequest request) {
		List<Board> boardList = boardService.findAll();
		return boardList;
	}

	@RequestMapping(value = "/subjects/{boardname}", method = RequestMethod.GET)
	@ResponseBody
	public List<Map<Integer, String>> home(Locale locale, Model model,
			@PathVariable(value = "boardname") String boardname,
			HttpServletRequest request) {
		Board board = boardService.findByName(boardname);
		List<Map<Integer, String>> subjectList = subjectService
				.findSubjectByBoard(board.getId().intValue());
		return subjectList;
	}

	@RequestMapping(value = "/subject/{boardid}", method = RequestMethod.GET)
	@ResponseBody
	public List<Subject> home(Locale locale, Model model,
			@PathVariable(value = "boardid") Long boardid,
			HttpServletRequest request) {
		Board board = boardService.findById(boardid);
		List<Subject> subjectList = subjectService.findByBoard(board);
		return subjectList;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@ResponseBody
	public List<Board> home(Locale locale, Model model,
			HttpServletRequest request) {
		List<Board> boardList = boardService.findAll();
		return boardList;
	}

	@RequestMapping(value = "/subjects/topics/{subjectid}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public RestResponse topics(Locale locale, Model model,
			@PathVariable(value = "subjectid") Long subjectid,
			HttpServletRequest request) {
		Subject subject = subjectService.findSubject(subjectid);
		RestResponse response = new RestResponse();
		Map<String, Object> questionSet = new HashMap<String, Object>(0);
		String subjectSessionId = RandomUtils.randomAlphanumeric(25);
		questionSet.put("sessionSubject", subjectSessionId);
		questionSet.put("sessionValues", subject);
		questionSet.put("subjectid", subject.getId());
		response.setObject(questionSet);
		return response;
	}
}
