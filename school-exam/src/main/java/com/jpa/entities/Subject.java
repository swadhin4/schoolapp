package com.jpa.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.annotate.JsonProperty;

@Table(name = "subjects")
@Entity
@JsonAutoDetect(JsonMethod.NONE)
public class Subject implements Serializable {

	@JsonProperty
	private Long id;
	@JsonProperty
	private String name;

	@JsonProperty
	private List<Topic> topics = new ArrayList<Topic>(0);
	@JsonProperty
	private Board board;
	@JsonProperty
	private Long maxQuestion;

	@JsonProperty
	private Date createdAt = new Date();
	@JsonProperty
	private int version;

	public Subject() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	@Column(name = "name", length = 20)
	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "subject")
	public List<Topic> getTopics() {
		return topics;
	}

	public void setTopics(final List<Topic> topics) {
		this.topics = topics;
	}

	@Column(name = "max_question")
	public Long getMaxQuestion() {
		return maxQuestion;
	}

	public void setMaxQuestion(Long maxQuestion) {
		this.maxQuestion = maxQuestion;
	}

	@ManyToOne
	@JoinColumn(name = "board_id")
	public Board getBoard() {
		return board;
	}

	public void setBoard(final Board board) {
		this.board = board;
	}

	@Column(name = "created_at")
	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(final Date createdAt) {
		this.createdAt = createdAt;
	}

	@Column(name = "version")
	@Version
	public int getVersion() {
		return version;
	}

	public void setVersion(final int version) {
		this.version = version;
	}

}
