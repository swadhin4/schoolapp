/* Copyright (C) 2013 , Inc. All rights reserved */
package com.jpa.entities;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * User.
 * 
 * @author Swadhin
 */
@Entity
@Table(name = "application_users")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@JsonAutoDetect(JsonMethod.NONE)
@Cacheable
public class User implements BaseEntity {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3228774824582399072L;

	/** The id. */
	@JsonProperty
	private Long id;

	/** The version. */
	@JsonIgnore
	private Integer version;

	/** The user name. */
	@JsonProperty
	private String userName;

	/** The password. */
	private String password;

	/** The first name. */

	/** The enabled. */
	@JsonProperty
	private Boolean enabled;

	/** The created at. */
	@JsonProperty
	private Date createdAt = new Date();

	/** The user role. */
	@JsonIgnore
	private UserRole userRole;

	private Student student;

	/** The role id. */
	@JsonProperty
	private Long roleId;

	/**
	 * Instantiates a new user.
	 */
	public User() {
	}

	/**
	 * Instantiates a new user.
	 * 
	 * @param createdAt
	 *            the created at
	 */
	public User(final Date createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * Instantiates a new user.
	 * 
	 * @param username
	 *            the username
	 * @param password
	 *            the password
	 * @param firstName
	 *            the first name
	 * @param lastName
	 *            the last name
	 * @param email
	 *            the email
	 * @param enabled
	 *            the enabled
	 * @param createdAt
	 *            the created at
	 * @param userRole
	 *            the user role
	 * @param phone
	 *            the phone
	 */
	public User(final String username, final String password,
			final Boolean enabled, final Date createdAt, final UserRole userRole) {
		userName = username;
		this.password = password;
		this.enabled = enabled;
		this.createdAt = createdAt;
		this.userRole = userRole;
	}

	/**
	 * Instantiates a new user.
	 * 
	 * @param userName
	 *            the user name
	 * @param password
	 *            the password
	 * @param firstName
	 *            the first name
	 * @param lastName
	 *            the last name
	 * @param email
	 *            the email
	 * @param enabled
	 *            the enabled
	 * @param phone
	 *            the phone
	 */
	public User(final String userName, final String password,
			final Boolean enabled) {
		super();
		this.userName = userName;
		this.password = password;
		this.enabled = enabled;
	}

	public User(String currentUser) {
		this.userName = currentUser;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	@Override
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the new id
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * Gets the user name.
	 * 
	 * @return the user name
	 */
	@Column(name = "username", length = 250)
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 * 
	 * @param userName
	 *            the new user name
	 */
	public void setUserName(final String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the password.
	 * 
	 * @return the password
	 */
	@Column(name = "password", length = 250)
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 * 
	 * @param password
	 *            the new password
	 */
	public void setPassword(final String password) {
		this.password = password;
	}

	@Column(name = "enabled")
	public Boolean getEnabled() {
		return enabled;
	}

	/**
	 * Sets the enabled.
	 * 
	 * @param enabled
	 *            the new enabled
	 */
	public void setEnabled(final Boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * Gets the created at.
	 * 
	 * @return the created at
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at", nullable = false, length = 19, updatable = false)
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * Sets the created at.
	 * 
	 * @param createdAt
	 *            the new created at
	 */
	public void setCreatedAt(final Date createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * Gets the version.
	 * 
	 * @return the version
	 */
	@Version
	@Column(name = "version")
	public Integer getVersion() {
		return version;
	}

	/**
	 * Sets the version.
	 * 
	 * @param version
	 *            the new version
	 */
	public void setVersion(final Integer version) {
		this.version = version;
	}

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "student_id")
	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	/**
	 * Gets the user role.
	 * 
	 * @return the user role
	 */
	@OneToOne(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.ALL)
	public UserRole getUserRole() {
		return userRole;
	}

	/**
	 * Sets the user role.
	 * 
	 * @param userRole
	 *            the new user role
	 */
	public void setUserRole(final UserRole userRole) {
		this.userRole = userRole;
		if (userRole != null && userRole.getRole() != null) {
			roleId = userRole.getId();
		}
	}

	/**
	 * Gets the role id.
	 * 
	 * @return the role id
	 */
	@Transient
	public Long getRoleId() {
		return roleId;
	}

	/**
	 * Sets the role id.
	 * 
	 * @param roleId
	 *            the new role id
	 */
	public void setRoleId(final Long roleId) {
		this.roleId = roleId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("User [");
		if (id != null) {
			builder.append("id=").append(id).append(", ");
		}

		if (userName != null) {
			builder.append("username=").append(userName).append(", ");
		}
		if (enabled != null) {
			builder.append("enabled=").append(enabled).append(", ");
		}
		if (createdAt != null) {
			builder.append("createdAt=").append(createdAt).append(", ");
		}
		if (roleId != null) {
			builder.append("roleId=").append(roleId).append(", ");
		}
		if (userRole != null) {
			builder.append("userRole=").append(userRole);
		}
		builder.append("]");
		return builder.toString();
	}

}
