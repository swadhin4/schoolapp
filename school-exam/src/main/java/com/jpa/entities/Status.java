package com.jpa.entities;

public enum Status {

	USED,

	NOT_USED,

	EXPIRED
}
