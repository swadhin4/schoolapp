package com.jpa.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jpa.entities.Board;

public interface BoardDAO extends JpaRepository<Board, Long> {

	Board findByName(String name);
}
