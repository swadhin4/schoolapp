package com.jpa.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jpa.entities.Subject;
import com.jpa.entities.Topic;

public interface TopicDAO extends JpaRepository<Topic, Long> {

	List<Topic> findBySubject(Subject subject);

}
