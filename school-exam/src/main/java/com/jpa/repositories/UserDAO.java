/* Copyright (C) 2013 , Inc. All rights reserved */
package com.jpa.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jpa.entities.User;

public interface UserDAO extends JpaRepository<User, Long> {

	// @Query(value =
	// "select * from application_users where username=:username", nativeQuery =
	// true)
	public User findByUserName(String username);
}
