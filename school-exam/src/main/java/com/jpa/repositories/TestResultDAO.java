package com.jpa.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jpa.entities.TestResult;
import com.jpa.entities.User;

public interface TestResultDAO extends JpaRepository<TestResult, Long> {

	List<TestResult> findByUser(User user);
}
