package com.jpa.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jpa.entities.Coupon;

public interface CouponDAO extends JpaRepository<Coupon, Long> {

	public Coupon findByCouponCode(String coupon);
}
