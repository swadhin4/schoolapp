package com.jpa.repositories;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.jpa.entities.Board;
import com.jpa.entities.Subject;

public interface SubjectDAO extends JpaRepository<Subject, Long> {

	@Query(value = "select id, name from subjects where board_id=:id", nativeQuery = true)
	public List<Map<Integer, String>> findBoardSubjects(@Param("id") Long id);

	public List<Subject> findByBoard(Board board);

}
