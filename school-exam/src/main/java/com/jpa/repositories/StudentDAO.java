package com.jpa.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jpa.entities.Student;

public interface StudentDAO extends JpaRepository<Student, Long> {

}
