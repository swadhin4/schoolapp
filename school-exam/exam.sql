-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.0.18-nt - MySQL Community Edition (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             8.0.0.4396
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for exam_db
DROP DATABASE IF EXISTS `exam_db`;
CREATE DATABASE IF NOT EXISTS `exam_db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `exam_db`;


-- Dumping structure for table exam_db.address
DROP TABLE IF EXISTS `address`;
CREATE TABLE IF NOT EXISTS `address` (
  `address_id` int(11) NOT NULL auto_increment,
  `address_line1` varchar(500) character set latin1 default NULL,
  `city` varchar(50) character set latin1 default NULL,
  `zipcode` varchar(50) character set latin1 default NULL,
  `state` varchar(50) character set latin1 default NULL,
  `created_at` datetime default NULL,
  `version` int(11) NOT NULL,
  PRIMARY KEY  (`address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table exam_db.address: ~1 rows (approximately)
DELETE FROM `address`;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` (`address_id`, `address_line1`, `city`, `zipcode`, `state`, `created_at`, `version`) VALUES
	(13, 'Joda', 'BBSR', '758034', 'ODISHA', '2015-09-07 00:49:01', 0);
/*!40000 ALTER TABLE `address` ENABLE KEYS */;


-- Dumping structure for table exam_db.application_users
DROP TABLE IF EXISTS `application_users`;
CREATE TABLE IF NOT EXISTS `application_users` (
  `user_id` int(11) NOT NULL auto_increment,
  `username` varchar(250) character set latin1 default NULL,
  `password` varchar(250) character set latin1 default NULL,
  `student_id` int(11) default NULL,
  `enabled` int(11) default NULL,
  `created_at` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `version` int(11) default NULL,
  PRIMARY KEY  (`user_id`),
  UNIQUE KEY `username_uq_cons` (`username`),
  KEY `FK_application_users_student` (`student_id`),
  CONSTRAINT `FK_application_users_student` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table exam_db.application_users: ~4 rows (approximately)
DELETE FROM `application_users`;
/*!40000 ALTER TABLE `application_users` DISABLE KEYS */;
INSERT INTO `application_users` (`user_id`, `username`, `password`, `student_id`, `enabled`, `created_at`, `version`) VALUES
	(1, 'root', 'root', NULL, 1, '2015-09-07 00:23:58', 0),
	(12, 'swadhin4@gmail.com', '9WG2MtqQ', 13, 1, '2016-02-21 02:27:55', 0),
	(13, 'tapesh@gmail.com', 'xZ4OjJhe', 15, 1, '2016-03-03 13:30:08', 0),
	(14, NULL, 'zTDGDHbG', 16, 1, '2016-03-13 17:13:33', 0);
/*!40000 ALTER TABLE `application_users` ENABLE KEYS */;


-- Dumping structure for table exam_db.board
DROP TABLE IF EXISTS `board`;
CREATE TABLE IF NOT EXISTS `board` (
  `id` int(10) NOT NULL auto_increment,
  `name` varchar(50) character set latin1 default NULL,
  `created_at` timestamp NULL default NULL,
  `version` int(10) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table exam_db.board: ~3 rows (approximately)
DELETE FROM `board`;
/*!40000 ALTER TABLE `board` DISABLE KEYS */;
INSERT INTO `board` (`id`, `name`, `created_at`, `version`) VALUES
	(1, 'ICSE', '2016-02-21 04:40:34', 0),
	(2, 'CBSE', '2016-02-21 04:40:34', 0),
	(3, 'HSE', '2016-02-21 04:40:34', 0);
/*!40000 ALTER TABLE `board` ENABLE KEYS */;


-- Dumping structure for table exam_db.coupon
DROP TABLE IF EXISTS `coupon`;
CREATE TABLE IF NOT EXISTS `coupon` (
  `coupon_id` int(10) NOT NULL auto_increment,
  `coupon_code` varchar(50) default NULL,
  `validity_date` timestamp NULL default NULL,
  `status` varchar(50) default NULL,
  `balance` decimal(10,2) default NULL,
  `created_at` timestamp NULL default NULL,
  PRIMARY KEY  (`coupon_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table exam_db.coupon: ~0 rows (approximately)
DELETE FROM `coupon`;
/*!40000 ALTER TABLE `coupon` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupon` ENABLE KEYS */;


-- Dumping structure for table exam_db.question
DROP TABLE IF EXISTS `question`;
CREATE TABLE IF NOT EXISTS `question` (
  `id` int(10) NOT NULL auto_increment,
  `question` longtext character set utf8 collate utf8_bin,
  `option1` longtext character set latin1,
  `option2` longtext character set latin1,
  `option3` longtext character set latin1,
  `option4` longtext character set latin1,
  `order` int(11) default NULL,
  `answer` longtext character set latin1,
  `topic_id` int(10) default NULL,
  `created_at` datetime default NULL,
  `version` int(10) default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_question_topic` (`topic_id`),
  CONSTRAINT `FK_question_topic` FOREIGN KEY (`topic_id`) REFERENCES `topic` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table exam_db.question: ~2 rows (approximately)
DELETE FROM `question`;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` (`id`, `question`, `option1`, `option2`, `option3`, `option4`, `order`, `answer`, `topic_id`, `created_at`, `version`) VALUES
	(1, '&#2835;&#2849;&#2879;&#2870;&#2878;&#2864;&#32;&#2858;&#2893;&#2864;&#2853;&#2862;&#32;&#2864;&#2878;&#2844;&#2893;&#2911;&#2858;&#2878;&#2867;&#32;&#2861;&#2878;&#2860;&#2887;&#32;&#2837;&#2879;&#2831;&#32;&#2870;&#2858;&#2853;&#32;&#2858;&#2878;&#2848;&#32;&#2837;&#2864;&#2879;&#2853;&#2879;&#2866;&#2887;&#32;&#63;', '&#2838;&#46;&#32;&#2872;&#2878;&#2864;&#32;&#2837;&#2891;&#2847;&#2856;&#2893;&#32;&#2847;&#2887;&#2864;&#2887;&#2866;&#2891;', '&#2837;&#46;&#32;&#2872;&#2878;&#2864;&#2844;&#2856;&#2893;&#32;&#2821;&#2871;&#2893;&#2847;&#2879;&#2856;&#2893;&#32;&#2873;&#2860;&#2878;&#2837;&#2893;', '&#2839;&#46;&#32;&#2872;&#2878;&#2864;&#32;&#2822;&#2823;&#2860;&#2878;&#2837;&#2893;&#32;&#2872;&#2893;&#2862;&#2879;&#2853;', '&#2840;&#46;&#32;&#2872;&#2878;&#2864;&#32;&#2847;&#2879;&#46;&#32;&#2822;&#2864;&#46;&#2859;&#2881;&#2837;&#2856;', 1, 'kha', 1, '2015-03-11 02:53:21', 0),
	(2, 'à¬“à¬¡à¬¿à¬¶à¬¾à¬° à¬ªà­à¬°à¬¥à¬® à¬°à¬¾à¬œà­à­Ÿà¬ªà¬¾à¬³ à¬­à¬¾à¬¬à­‡ à¬•à¬¿à¬ à¬¶à¬ªà¬¥ à¬ªà¬¾à¬  à¬•à¬°à¬¿à¬¥à¬¿à¬²à­‡ ? ', 'à¬–. à¬¸à¬¾à¬° à¬•à­‹à¬Ÿà¬¨à­ à¬Ÿà­‡à¬°à­‡à¬²à­‹', 'à¬–. à¬¸à¬¾à¬° à¬•à­‹à¬Ÿà¬¨à­ à¬Ÿà­‡à¬°à­‡à¬²à­‹ ', 'à¬–. à¬¸à¬¾à¬° à¬•à­‹à¬Ÿà¬¨à­ à¬Ÿà­‡à¬°à­‡à¬²à­‹', 'à¬–. à¬¸à¬¾à¬° à¬•à­‹à¬Ÿà¬¨à­ à¬Ÿà­‡à¬°à­‡à¬²à­‹', 1, 'kha', 1, '2015-03-11 02:53:21', 0);
/*!40000 ALTER TABLE `question` ENABLE KEYS */;


-- Dumping structure for table exam_db.role
DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(250) character set latin1 default NULL,
  `description` varchar(250) character set latin1 default NULL,
  `role_name` varchar(100) character set latin1 default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table exam_db.role: ~2 rows (approximately)
DELETE FROM `role`;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`id`, `name`, `description`, `role_name`) VALUES
	(1, 'Admin', 'Admin User', 'ROLE_ADMIN'),
	(2, 'User', 'Normal User', 'ROLE_USER');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;


-- Dumping structure for table exam_db.standard
DROP TABLE IF EXISTS `standard`;
CREATE TABLE IF NOT EXISTS `standard` (
  `id` int(10) default NULL,
  `standard` varchar(50) character set latin1 default NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table exam_db.standard: ~0 rows (approximately)
DELETE FROM `standard`;
/*!40000 ALTER TABLE `standard` DISABLE KEYS */;
/*!40000 ALTER TABLE `standard` ENABLE KEYS */;


-- Dumping structure for table exam_db.student
DROP TABLE IF EXISTS `student`;
CREATE TABLE IF NOT EXISTS `student` (
  `id` int(10) NOT NULL auto_increment,
  `first_name` varchar(50) character set latin1 default NULL,
  `last_name` varchar(50) character set latin1 default NULL,
  `email` varchar(50) character set latin1 default NULL,
  `mobile` varchar(50) character set latin1 default NULL,
  `institute_name` varchar(50) character set latin1 default NULL,
  `board` varchar(50) character set latin1 default NULL,
  `standard` varchar(50) character set latin1 default NULL,
  `balance` decimal(10,2) default NULL,
  `coupon_code` varchar(50) collate utf8_unicode_ci default NULL,
  `created_at` timestamp NULL default CURRENT_TIMESTAMP,
  `version` int(11) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table exam_db.student: ~4 rows (approximately)
DELETE FROM `student`;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` (`id`, `first_name`, `last_name`, `email`, `mobile`, `institute_name`, `board`, `standard`, `balance`, `coupon_code`, `created_at`, `version`) VALUES
	(13, 'Swadhin', 'Mohanta', 'swadhin4@gmail.com', '9583291103', 'ST Teresa', 'ICSE', '10', NULL, NULL, '2016-02-21 02:27:55', 0),
	(14, 'Tapesh', 'Das', 'ssdd', '1334556', 'NIST', 'BPUT', '12', NULL, NULL, '2016-02-21 02:29:25', 0),
	(15, 'Tapesh', 'Das', 'tapesh@gmail.com', '1234567890', 'BBS', 'CBSE', '2', NULL, NULL, '2016-03-03 13:30:08', 0),
	(16, 'fs', 'fs', NULL, 'fdf', 'fsd', 'fs', 'fs', NULL, NULL, '2016-03-13 17:13:33', 0);
/*!40000 ALTER TABLE `student` ENABLE KEYS */;


-- Dumping structure for table exam_db.subjects
DROP TABLE IF EXISTS `subjects`;
CREATE TABLE IF NOT EXISTS `subjects` (
  `id` int(10) NOT NULL auto_increment,
  `name` varchar(50) character set latin1 default NULL,
  `board_id` int(10) default NULL,
  `standard` varchar(50) character set latin1 default NULL,
  `created_at` timestamp NULL default NULL,
  `version` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_subjects_board` (`board_id`),
  CONSTRAINT `FK_subjects_board` FOREIGN KEY (`board_id`) REFERENCES `board` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table exam_db.subjects: ~5 rows (approximately)
DELETE FROM `subjects`;
/*!40000 ALTER TABLE `subjects` DISABLE KEYS */;
INSERT INTO `subjects` (`id`, `name`, `board_id`, `standard`, `created_at`, `version`) VALUES
	(1, 'PHYSICS', 1, NULL, NULL, 0),
	(2, 'CHEMISTRY', 1, NULL, NULL, 0),
	(3, 'MATHEMATICS', 1, NULL, NULL, 0),
	(4, 'HISTORY', 1, NULL, NULL, 0),
	(5, 'GEOGRAPHY', 1, NULL, NULL, 0);
/*!40000 ALTER TABLE `subjects` ENABLE KEYS */;


-- Dumping structure for table exam_db.test_result
DROP TABLE IF EXISTS `test_result`;
CREATE TABLE IF NOT EXISTS `test_result` (
  `id` int(10) NOT NULL auto_increment,
  `user_id` int(10) default NULL,
  `subject_id` int(10) default NULL,
  `score` int(10) default NULL,
  `attempt` int(10) default NULL,
  `questions_done` varchar(50) character set latin1 default NULL,
  `wrong_answer` varchar(50) character set latin1 default NULL,
  `right_answer` varchar(50) character set latin1 default NULL,
  `created_at` timestamp NULL default CURRENT_TIMESTAMP,
  `version` int(10) default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_test_result_application_users` (`user_id`),
  KEY `FK_test_result_subjects` (`subject_id`),
  CONSTRAINT `FK_test_result_application_users` FOREIGN KEY (`user_id`) REFERENCES `application_users` (`user_id`),
  CONSTRAINT `FK_test_result_subjects` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table exam_db.test_result: ~0 rows (approximately)
DELETE FROM `test_result`;
/*!40000 ALTER TABLE `test_result` DISABLE KEYS */;
/*!40000 ALTER TABLE `test_result` ENABLE KEYS */;


-- Dumping structure for table exam_db.topic
DROP TABLE IF EXISTS `topic`;
CREATE TABLE IF NOT EXISTS `topic` (
  `id` int(10) NOT NULL auto_increment,
  `name` varchar(50) character set latin1 default NULL,
  `subject_id` int(11) default NULL,
  `standard` int(11) default NULL,
  `created_at` timestamp NULL default NULL,
  `version` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_topic_subjects` (`subject_id`),
  CONSTRAINT `FK_topic_subjects` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table exam_db.topic: ~4 rows (approximately)
DELETE FROM `topic`;
/*!40000 ALTER TABLE `topic` DISABLE KEYS */;
INSERT INTO `topic` (`id`, `name`, `subject_id`, `standard`, `created_at`, `version`) VALUES
	(1, 'Measurement of Basic Elements', 1, 7, '2016-02-21 04:48:32', 0),
	(2, 'Force and Motion', 1, 7, '2016-02-21 04:53:06', 0),
	(3, 'Elements and Compunds', 2, 7, '2016-02-21 04:53:43', 0),
	(4, 'Air and its Constituents', 2, 7, '2016-02-21 04:54:10', 0);
/*!40000 ALTER TABLE `topic` ENABLE KEYS */;


-- Dumping structure for table exam_db.user_role
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE IF NOT EXISTS `user_role` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) default NULL,
  `role_id` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `user_role_ibfk_1` (`role_id`),
  KEY `user_role_ibfk_2` (`user_id`),
  CONSTRAINT `FK_user_role_application_users` FOREIGN KEY (`user_id`) REFERENCES `application_users` (`user_id`),
  CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table exam_db.user_role: ~4 rows (approximately)
DELETE FROM `user_role`;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` (`id`, `user_id`, `role_id`) VALUES
	(1, 1, 1),
	(2, 12, 2),
	(3, 13, 2),
	(4, 14, 2);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
